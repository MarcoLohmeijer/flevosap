<?php
$router->get('', 'HomeController@getHome');
$router->get('home', 'HomeController@getHome');

$router->get('login', 'UserController@getLogin');
$router->post('action/login-user', 'LoginAction@login');

$router->get('register', 'UserController@getRegister');
$router->post('action/create-user', 'RegisterAction@register');

$router->get('action/logout-user', 'UserController@logout');

$router->get('Update-User', 'UserController@getUpdate');
$router->post('action/update-user', 'UpdateUserAction@update');

$router->get('change-password', 'UserController@getChangePassword');
$router->post('action/UpdatePassword', 'UpdatePasswordAction@update');

$router->get('DeleteUser', 'UserController@getDelete');
$router->post('action/deleteUser', 'DeleteUserAction@delete');

$router->get('privacy', 'HomeController@getPrivacy');
$router->get('Contact', 'HomeController@getContact');

$router->get('items', 'ItemController@getItemList');

$router->get('shopping-cart', 'ShoppingCartController@getShoppingCart');
$router->post('action/delete-item-from-cart', 'DeleteItemFromCartAction@delete');
$router->post('action/update-item-from-cart', 'UpdateItemInCartAction@update');
$router->post('action/add-item-to-cart', 'AddItemToCartAction@add');

$router->get('orderPayForm' , 'OrderController@orderPayForm');
$router->get('registerAdressForm', 'OrderController@registerAddressForm');
$router->get('action/comparUserAdresses', 'UserController@compareUserAdresses');
$router->post('action/create-address', 'AddUserAddressAction@add');

$router->post('action/migrateToOrderItems','MigrateToOrderItems@migrate');

$router->get('orders', 'OrderController@getOrdersView');

/** Admin pages */
$router->get('admin-overview' , 'AdminController@getAdminOverview');

$router->get('admin-order-overview' ,'AdminController@getOrderOverview');

$router->get('admin-item-list', 'AdminController@getItemList');
$router->get('edit-item', 'AdminController@getEditItem');
$router->post('action/delete_item', 'AdminDeleteItemAction@delete');
$router->post('action/edit-item', 'AdminEditItemAction@edit');

$router->post('admin-delete-user-action' , 'AdminDeleteUserAction@delete');

$router->get('admin-add-item', 'AdminController@getAddItem');
$router->post('actions/add_item','AdminAddItemAction@insert');

$router->get('admin-delete-user', 'AdminController@getDelete');
$router->post('action/AdminDeleteUser', 'AdminDeleteUserAction@delete');

$router->get('admin-add-account', 'AdminController@getAddAccount');
$router->post('admin-add-account-action', 'AdminAddUser@add');
