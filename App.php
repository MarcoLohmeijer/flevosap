<?php


class App
{
    /** @var array */
    protected static $reglist= [];

    /**
     * @param $key
     *
     * @param $value
     */
    public static function bind($key, $value)
    {
        static::$reglist[$key] = $value;
    }

    /**
     * @param $key
     *
     * @return mixed
     * @throws Exception
     */
    public function get($key)
    {
        if (! array_key_exists($key, static::$reglist)) {
            throw new Exception("No key is bound");
        }

        return static::$reglist[$key];
    }
}