<?php

use src\enums\Routes;

require "vendor/autoload.php";
$query = require "bootstrap.php";

$router = new Router();
require "routes.php";

$uri = trim($_SERVER['REQUEST_URI'], '/');

require Router::load('routes.php')
    ->direct(Request::uri(), Request::method());
