# Flevosap
  
###Requirements
  * PHP 7.3 +
  * MYSQL
  * Composer 1.9.0


To use this project you need to install composer you can do that [here](https://getcomposer.org/)

To download MYSQL you can do it [here](https://dev.mysql.com/downloads/installer/)

To download PHP 7.3 you can do it [here](https://windows.php.net/download/)

###Autoload 

If you add a class use the command `` composer dumpautoload`` the classes will then be auto loaded!

###Web-server

To start the webserver for PHP use ``php -S localhost:80`` or `` php -S localhost:6666``.

###Database settings
To set the settings of the database  go to the file `src/enums/DatabaseSettingsKopie.php`

rename the file to ` DatabaseSettings.php` by doing this in command line

Note: You need to be in the enums folder!

`cp DatabadeSettingsKoie.php DatabaseSettings.php`


 
Uncomment the classname in the file. 

Set your own database settings in the constants.