<!DOCTYPE html>
<html lang="en">
<?php

use src\controllers\OrderController;
use src\controllers\ShoppingCartController;
use src\enums\App;
use src\enums\Routes;

$w = new ShoppingCartController();
$orderController = new OrderController();

if (isset($_SESSION['user_id']) === false) {
    header("Location: " . App::URL . Routes::LOGIN . "?message=unauthorized");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php' ?>
<body>
<?php include_once 'src/components/navbar.php' ?>
<div class="container-fluid">
    <?php include_once 'src/components/banner.php' ?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <?php
            $number = 0;
            foreach ($w->getItems($_SESSION['user_id']) as $item) {
                $w->addToTotalCosts($item['price'] * $item['amount']);
                $number += 1;
                echo
                    "<div class='row'>
                                    <div class='col-sm-2'>
                                        <img class=\"card-img-top\" src=\"/src/item_images/" . $item['img_url'] . "\" alt=\"Card image\" style=\"max-width:100px; max-height:100px;\">
                                    </div>
                                    
                                    <div class='col-sm-6'>
                                        <b>" . $item['name'] . "</b>
                                        <p>" . $item['description'] . "</p>
                                    </div>
                                    
                                    <div class='col-sm-2'>
                                        <p>Prijs: &euro; " . number_format((float)$item['price'], 2, '.', '') . "</p>
                                    </div>
                                    
                                    <div class='col-sm-2'>
                                        <p>Aantal: " . $item['amount'] . "</p>
                                    </div>                                
                            </div>
                         <hr>";
            }
            ?>
            <form action="<?php echo Routes::MOVE_CART_TO_ORDER_ITEMS ?>" method="post">
                <input name="user_id" type="hidden" value='<?php echo $_SESSION['user_id']; ?>' class="form-control" id="user_id">
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="checkbox" name="deliver" value="true">Bezorgen
                    </label>
                </div>
                <div class="form-group">
                    <h2>Totaal bedrag = &euro;<?php echo $w->getCosts($_SESSION['user_id']); ?> </h2>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
</body>
</html>
