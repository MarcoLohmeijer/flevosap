<!DOCTYPE html>
<?php use src\enums\Routes; ?>

<?php include_once 'src/components/head.php';?>
<body>
<?php include_once 'src/components/navbar.php'; ?>
<?php include_once "src/components/banner.php"; ?>

<?php include_once "src/components/CheckIfAdmin.php";?>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="row">

                    <div class="col-sm-4">
                        <div class="card m-5" style="width: 18rem;">
                            <div class="card-body card-hover">
                                <h5 class="card-title ">Items aanpassen</h5>
                                <p class="card-text">Items aanpassen en vewijderen</p>
                                <a href="<?php echo Routes::ADMIN_ITEM_LIST ?>" class="stretched-link"></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="card m-5 card-hover" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Items toevoegen</h5>
                                <p class="card-text">Items toevoegen</p>
                                <a href="<?php echo Routes::ADMIN_ADD_ITEM ?>" class="stretched-link"></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="card m-5 card-hover" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Gebruikers</h5>
                                <p class="card-text">Gebruikers inzien en vewijderen</p>
                                <a href="<?php echo Routes::ADMIN_DELETE_USER ?>" class="stretched-link"></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="card m-5 card-hover" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Gebruiker toevoegen</h5>
                                <p class="card-text">Gebruiker of admin toevoegen</p>
                                <a href="<?php echo Routes::ADMIN_ADD_ACCOUNT ?>" class="stretched-link"></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="card m-5 card-hover" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Orders</h5>
                                <p class="card-text">Overzicht van orders</p>
                                <a href="<?php echo Routes::ADMIN_ORDERS ?>" class="stretched-link"></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<div class="col-2"></div>
</body>

