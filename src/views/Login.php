<!DOCTYPE html>
<html lang="en">
<?php

use src\enums\Routes;

include_once "src/components/head.php"; ?>
<body>

<?php include_once "src/components/navbar.php"; ?>

<div class="container-fluid">
    <?php include_once "src/components/banner.php"; ?>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h1>Login</h1>
            <form action="<?php echo Routes::LOGIN_USER ?>" method="post">
                <div class="form-group">
                    <label for="usr">Name:</label>
                    <input name="username" type="text" class="form-control " id="usr" >
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input name="password" type="password" class="form-control" id="pwd">
                </div>
                <div class="float-right">
                    </h4>
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </form>
            <i>Nog geen account? <a href="<?php echo Routes::REGISTER ?>">register</a></i>
            <?php include_once "src/components/errorMessage.php"; ?>
        </div>
        <div class="col-sm-4"></div>
    </div>
</div>
</body>
</html>