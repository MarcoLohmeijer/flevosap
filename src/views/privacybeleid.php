<!DOCTYPE html>
<?php include_once 'src/components/head.php' ?>

<body>
<?php include_once 'src/components/navbar.php' ?>
<div class="container-fluid">

    <?php include_once "src/components/banner.php" ?>
    <div class="row">
        <div class="col" style="background-color: white;">
            <div class="container"><img
                        src="https://www.apple-co.eu/wp-content/uploads/2016/03/Assortimentslider-bomen-links-1.png"
                        class="rounded" alt="Cinque Terre" height="450"
                        width="350"></div>
        </div>
        <div class="col" style="background-color: white;">
            <h1 style="color:darkred">Privacybeleid Flevosap!</h1>
            <p style="color:darkgreen">
            <h2>Over ons privacybeleid</h2><br>
            Flevosap geeft veel om uw privacy. Wij verwerken daarom uitsluitend gegevens die wij nodig hebben voor (het
            verbeteren van) onze dienstverlening en gaan zorgvuldig om met de informatie die wij over u en uw gebruik
            van onze diensten hebben verzameld. Wij stellen uw gegevens nooit voor commerciële doelstellingen ter
            beschikking aan derden.
            Dit privacybeleid is van toepassing op het gebruik van de website en de daarop ontsloten dienstverlening van
            Flevosap. De ingangsdatum voor de geldigheid van deze voorwaarden is 09/10/2019, met het publiceren van een
            nieuwe versie vervalt de geldigheid van alle voorgaande versies. Dit privacybeleid beschrijft welke gegevens
            over u door ons worden verzameld, waar deze gegevens voor worden gebruikt en met wie en onder welke
            voorwaarden deze gegevens eventueel met derden kunnen worden gedeeld. Ook leggen wij aan u uit op welke
            wijze wij uw gegevens opslaan en hoe wij uw gegevens tegen misbruik beschermen en welke rechten u heeft met
            betrekking tot de door u aan ons verstrekte persoonsgegevens.
            Als u vragen heeft over ons privacybeleid kunt u contact opnemen met onze contactpersoon voor privacyzaken,
            u vindt de contactgegevens aan het einde van ons privacybeleid.
            <h2>Over de gegevensverwerking</h2>
            Hieronder kan u lezen op welke wijze wij uw gegevens verwerken, waar wij deze (laten) opslaan, welke
            beveiligingstechnieken wij gebruiken en voor wie de gegevens inzichtelijk zijn.
            <h2>Webwinkelsoftware</h2>
            MijnWebwinkel<br>
            Onze webwinkel is ontwikkeld met software van MyOnlineStore. Persoonsgegevens die u ten behoeve van onze
            dienstverlening aan ons beschikbaar stelt, worden met deze partij gedeeld. MyOnlineStore heeft toegang tot
            uw gegevens om ons (technische) ondersteuning te bieden, zij zullen uw gegevens nooit gebruiken voor een
            ander doel. MyOnlineStore is op basis van de overeenkomst die wij met hen hebben gesloten verplicht om
            passende beveiligingsmaatregelen te nemen. MyOnlineStore maakt gebruik van cookies om technische informatie
            te verzamelen met betrekking tot uw gebruik van de software, er worden geen persoonsgegevens verzameld en/of
            opgeslagen.
            <h2>Webhosting</h2>
            MijnDomein<br>
            Wij nemen webhosting- en emaildiensten af van MijnDomein. MijnDomein verwerkt persoonsgegevens namens ons en
            gebruikt uw gegevens niet voor eigen doeleinden. Wel kan deze partij metagegevens verzamelen over het
            gebruik van de diensten. Dit zijn geen persoonsgegevens. MijnDomein heeft passende technische en
            organisatorische maatregelen genomen om verlies en ongeoorloofd gebruik van uw persoonsgegevens te
            voorkomen. MijnDomein is op grond van de overeenkomst tot geheimhouding verplicht.
            Wij maken voor ons reguliere zakelijke e-mailverkeer gebruik van de diensten van . Deze partij heeft
            passende technische en organisatorische maatregelen getroffen om misbruik, verlies en corruptie van uw en
            onze gegevens zoveel mogelijk te voorkomen. heeft geen toegang tot ons postvak en wij behandelen al ons
            e-mailverkeer vertrouwelijk.
            <h2>Payment processors</h2>
            <h2>Beoordelingen</h2>
            Feedback Company<br>
            Wij verzamelen reviews via het platform van Feedback Company. Als u een review achterlaat via Feedback
            Company dan bent u verplicht om uw naam, woonplaats en e-mailadres op te geven. Feedback Company deelt deze
            gegevens met ons, zodat wij de review aan uw bestelling kunnen koppelen. Feedback Company publiceert uw naam
            en woonplaats eveneens op de eigen website. In sommige gevallen kan Feedback Company contact met u opnemen
            om een toelichting op uw review te geven. In het geval dat wij u uitnodigen om een review achter te laten
            delen wij uw naam en e-mailadres en informatie met betrekking tot uw order met Feedback Company. Zij
            gebruiken deze gegevens enkel met het doel u uit te nodigen om een review achter te laten. Feedback Company
            heeft passende technische en organisatorische maatregelen genomen om uw persoonsgegevens te beschermen.
            Feedback Company behoudt zich het recht voor om ten behoeve van het leveren van de dienstverlening derden in
            te schakelen, hiervoor hebben wij aan Feedback Company toestemming gegeven. Alle hierboven genoemde
            waarborgen met betrekking tot de bescherming van uw persoonsgegevens zijn eveneens van toepassing op de
            onderdelen van de dienstverlening waarvoor Feedback Company derden inschakelt.
            <h2>Verzenden en logistiek</h2>
            PostNL<br>
            Als u een bestelling bij ons plaatst is het onze taak om uw pakket bij u te laten bezorgen. Wij maken
            gebruik van de diensten van PostNL voor het uitvoeren van de leveringen. Het is daarvoor noodzakelijk dat
            wij uw naam, adres en woonplaatsgegevens met PostNL delen. PostNL gebruikt deze gegevens alleen ten behoeve
            van het uitvoeren van de overeenkomst. In het geval dat PostNL onderaannemers inschakelt, stelt PostNL uw
            gegevens ook aan deze partijen ter beschikking.
            <h2>Facturatie en boekhouden</h2>
            FactuurSturen<br>
            Voor onze bijhouden van onze administratie en boekhouding maken wij gebruik van de diensten van
            FactuurSturen. Wij delen uw naam, adres en woonplaatsgegevens en details met betrekking tot uw bestelling.
            Deze gegevens worden gebruikt voor het administreren van verkoopfacturen. Uw persoonsgegevens worden
            beschermd verzonden en opgeslagen. FactuurSturen is tot geheimhouding verplicht en zal uw gegevens
            vertrouwelijk behandelen. FactuurSturen gebruikt uw persoonsgegevens niet voor andere doeleinden dan
            hierboven beschreven.
            Externe verkoopkanalen<br>
            <h2>Doel van de gegevensverwerking</h2>
            Algemeen doel van de verwerking<br>
            Wij gebruiken uw gegevens uitsluitend ten behoeve van onze dienstverlening. Dat wil zeggen dat het doel van
            de verwerking altijd direct verband houdt met de opdracht die u verstrekt. Wij gebruiken uw gegevens niet
            voor (gerichte) marketing. Als u gegevens met ons deelt en wij gebruiken deze gegevens om - anders dan op uw
            verzoek - op een later moment contact met u op te nemen, vragen wij u hiervoor expliciet toestemming. Uw
            gegevens worden niet met derden gedeeld, anders dan om aan boekhoudkundige en overige administratieve
            verplichtingen te voldoen. Deze derden zijn allemaal tot geheimhouding gehouden op grond van de overeenkomst
            tussen hen en ons of een eed of wettelijke verplichting.
            Automatisch verzamelde gegevens<br>
            Gegevens die automatisch worden verzameld door onze website worden verwerkt met het doel onze
            dienstverlening verder te verbeteren. Deze gegevens (bijvoorbeeld uw IP-adres, webbrowser en
            besturingssysteem) zijn geen persoonsgegevens.
            Medewerking aan fiscaal en strafrechtelijk onderzoek<br>
            In voorkomende gevallen kan Flevosap op grond van een wettelijke verplichting worden gehouden tot het delen
            van uw gegevens in verband met fiscaal of strafrechtelijk onderzoek van overheidswege. In een dergelijk
            geval zijn wij gedwongen uw gegevens te delen, maar wij zullen ons binnen de mogelijkheden die de wet ons
            biedt daartegen verzetten.
            Bewaartermijnen<br>
            Wij bewaren uw gegevens zolang u cliënt van ons bent. Dit betekent dat wij uw klantprofiel bewaren totdat u

            aangeeft dat u niet langer van onze diensten gebruik wenst te maken. Als u dit bij ons aangeeft zullen wij
            dit tevens opvatten als een vergeetverzoek. Op grond van toepasselijke administratieve verplichtingen dienen
            wij facturen met uw (persoons)gegevens te bewaren, deze gegevens zullen wij dus voor zolang de toepasselijke
            termijn loopt bewaren. Medewerkers hebben echter geen toegang meer tot uw cliëntprofiel en documenten die
            wij naar aanleiding van uw opdracht hebben vervaardigd.
            <h2>Uw rechten</h2>
            Op grond van de geldende Nederlandse en Europese wetgeving heeft u als betrokkene bepaalde rechten met
            betrekking tot de persoonsgegevens die door of namens ons worden verwerkt. Wij leggen u hieronder uit welke
            rechten dit zijn en hoe u zich op deze rechten kunt beroepen. In beginsel sturen wij om misbruik te
            voorkomen afschriften en kopieën van uw gegevens enkel naar uw bij ons reeds bekende e-mailadres. In het
            geval dat u de gegevens op een ander e-mailadres of bijvoorbeeld per post wenst te ontvangen, zullen wij u
            vragen zich te legitimeren. Wij houden een administratie bij van afgehandelde verzoeken, in het geval van
            een vergeetverzoek administreren wij geanonimiseerde gegevens. Alle afschriften en kopieën van gegevens
            ontvangt u in de machineleesbare gegevensindeling die wij binnen onze systemen hanteren. U heeft te allen
            tijde het recht om een klacht in te dienen bij de Autoriteit Persoonsgegevens als u vermoedt dat wij uw
            persoonsgegevens op een verkeerde manier gebruiken.
            Inzagerecht<br>
            U heeft altijd het recht om de gegevens die wij (laten) verwerken en die betrekking hebben op uw persoon of
            daartoe herleidbaar zijn, in te zien. U kunt een verzoek met die strekking doen aan onze contactpersoon voor
            privacyzaken. U ontvangt dan binnen 30 dagen een reactie op uw verzoek. Als uw verzoek wordt ingewilligd
            sturen wij u op het bij ons bekende e-mailadres een kopie van alle gegevens met een overzicht van de
            verwerkers die deze gegevens onder zich hebben, onder vermelding van de categorie waaronder wij deze
            gegevens hebben opgeslagen.
            Rectificatierecht<br>
            U heeft altijd het recht om de gegevens die wij (laten) verwerken en die betrekking hebben op uw persoon of
            daartoe herleidbaar zijn, te laten aanpassen. U kunt een verzoek met die strekking doen aan onze
            contactpersoon voor privacyzaken. U ontvangt dan binnen 30 dagen een reactie op uw verzoek. Als uw verzoek
            wordt ingewilligd sturen wij u op het bij ons bekende e-mailadres een bevestiging dat de gegevens zijn
            aangepast.
            Recht op beperking van de verwerking<br>
            U heeft altijd het recht om de gegevens die wij (laten) verwerken die betrekking hebben op uw persoon of
            daartoe herleidbaar zijn, te beperken. U kunt een verzoek met die strekking doen aan onze contactpersoon
            voor privacyzaken. U ontvangt dan binnen 30 dagen een reactie op uw verzoek. Als uw verzoek wordt
            ingewilligd sturen wij u op het bij ons bekende e-mailadres een bevestiging dat de gegevens tot u de
            beperking opheft niet langer worden verwerkt.
            Recht op overdraagbaarheid<br>
            U heeft altijd het recht om de gegevens die wij (laten) verwerken en die betrekking hebben op uw persoon of
            daartoe herleidbaar zijn, door een andere partij te laten uitvoeren. U kunt een verzoek met die strekking
            doen aan onze contactpersoon voor privacyzaken. U ontvangt dan binnen 30 dagen een reactie op uw verzoek.
            Als uw verzoek wordt ingewilligd sturen wij u op het bij ons bekende e-mailadres afschriften of kopieën van
            alle gegevens over u die wij hebben verwerkt of in opdracht van ons door andere verwerkers of derden zijn
            verwerkt. Naar alle waarschijnlijkheid kunnen wij in een dergelijk geval de dienstverlening niet langer
            voortzetten, omdat de veilige koppeling van databestanden dan niet langer kan worden gegarandeerd.
            Recht van bezwaar en overige rechten<br>
            U heeft in voorkomende gevallen het recht bezwaar te maken tegen de verwerking van uw persoonsgegevens door
            of in opdracht van Flevosap. Als u bezwaar maakt zullen wij onmiddellijk de gegevensverwerking staken in
            afwachting van de afhandeling van uw bezwaar. Is uw bezwaar gegrond dat zullen wij afschriften en/of
            kopieën van gegevens die wij (laten) verwerken aan u ter beschikking stellen en daarna de verwerking
            blijvend staken. U heeft bovendien het recht om niet aan geautomatiseerde individuele besluitvorming of
            profiling te worden onderworpen. Wij verwerken uw gegevens niet op zodanige wijze dat dit recht van
            toepassing is. Bent u van mening dat dit wel zo is, neem dan contact op met onze contactpersoon voor
            privacyzaken.
            <h2>Cookies</h2>
            Cookies van derde partijen<br>
            In het geval dat softwareoplossingen van derde partijen gebruik maken van cookies is dit vermeld in deze

            privacyverklaring.
            <h2>Wijzigingen in het privacybeleid</h2>
            Wij behouden te allen tijde het recht ons privacybeleid te wijzigen. Op deze pagina vindt u echter altijd de
            meest recente versie. Als het nieuwe privacybeleid gevolgen heeft voor de wijze waarop wij reeds verzamelde
            gegevens met betrekking tot u verwerken, dan brengen wij u daarvan per e-mail op de hoogte.
            <br>
            <h2>Contactgegevens</h2>
            Flevosap
            Prof. Zuurlaan 22 8256 PE Biddinghuizen Nederland T (310) 321-3325 E info@flevosap.nl<br>
            Contactpersoon voor privacyzaken<br>
            Flevosap</p>
        </div>
        <div class="col" style="background-color: white;">
            <div class="container"><img
                        src="https://www.apple-co.eu/wp-content/uploads/2016/03/Assortimentslider-bomen-rechts.png"
                        class="rounded" alt="Cinque Terre" height="450"
                        width="350">
            </div>
        </div>
    </div>
</div>
</body>

</html>