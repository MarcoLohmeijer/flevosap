<!DOCTYPE html>
<?php use src\enums\Routes;

include_once 'src/components/head.php'; ?>

<body>
<?php include_once 'src/components/navbar.php'; ?>
<div class="container-fluid">
<?php include_once "src/components/banner.php"; ?>

    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h1 class="text-center">User settings</h1>

            <form action="<?php echo Routes::UPDATE_USER ?>" method="POST">

                <div class="form-group">
                    <label for="usr">User name:</label>
                    <input value="<?php echo $_SESSION['username'] ?>" name="username" type="text" class="form-control"
                           id="usr">
                </div>

                <div class="form-group">
                    <label for="email">E-mail:</label>
                    <input value="<?php echo $_SESSION['email'] ?>" name="email" type="text" class="form-control"
                           id="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Current password:</label>
                    <input name="password" type="password" class="form-control" id="pwd">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo Routes::CHANGE_PASSWORD ?>" class="btn btn-primary float-right" role="button">Change password</a>
            </form>
            <br>
            <a href="<?php echo Routes::DELETE_USER_PAGE ?>" class="btn btn-primary float-right" role="button">Delete account</a>
			<?php include_once "src/components/errorMessage.php"; ?>
        </div>

        <div class="col-4"></div>
    </div>
</div>
</body>
</html>