<!DOCTYPE html>
<?php use src\enums\Routes;

include_once 'src/components/head.php'; ?>

<body>
<?php include_once 'src/components/navbar.php'; ?>
<?php include_once "src/components/banner.php"; ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h1 class="text-center">Delete your account</h1>

            <form action="<?php echo Routes::DELETE_USER ?>" method="POST">

                <div class="form-group">
                    <label for="usr">User name:</label>
                    <input value="<?php echo $_SESSION['username'] ?>" name="username" type="text" class="form-control"
                           id="usr">
                </div>

                <div class="form-group">
                    <label for="pwd">Current password:</label>
                    <input name="password" type="password" class="form-control" id="pwd">
                </div>
                <button type="submit" class="btn btn-primary">Delete</button>
                <a href="<?php echo Routes::USER_SETTINGS ?>" class="btn btn-primary float-right" role="button">Return
                    to previous page</a>
            </form>
        </div>
        <div class="col-4"></div>
    </div>
</div>
</body>