<!DOCTYPE html>
<?php include_once 'src/components/head.php' ?>

<body>
<?php include_once 'src/components/navbar.php' ?>
<div class="container-fluid">
    <?php include_once "src/components/banner.php" ?>
    <div class="row">
        <div class="col" style="background-color: white;">
            <div class="container"><img
                        src="https://www.apple-co.eu/wp-content/uploads/2016/03/Assortimentslider-bomen-links-1.png"
                        class="rounded" alt="Cinque Terre" height="450"
                        width="350"></div>
        </div>
        <div class="col" style="background-color: white;">
            <h1 style="color:darkred">Contact</h1>
            <p style="color:darkgreen">
                Flevosap bv<br>
                Prof. Zuurlaan 22<br>
                8256 PE Biddinghuizen, Nederland<br>
                Telefoon: +31 (0)321 – 33 25 25<br>
                E-mail: info@flevosap.nl<br>
                <br>
                KvK 58224483<br>
                BTW NL8529.322.73.B.01<br>
            </p>
            <h1 style="color:darkred">DISCLAIMER!</h1>
            <p style="color:darkgreen">
                Deze website is niet gemaakt door de echte flevosap. Ook niet in samenwerking met.<br>
                Dit is een schoolopdracht gemaakt door leerlingen van het Windesheim Flevoland<br>
                <br>
                Marco, Nick, Fatih en Daniël. -> Groep 10B
            </p>

        </div>
        <div class="col" style="background-color: white;">
            <div class="container"><img
                        src="https://www.apple-co.eu/wp-content/uploads/2016/03/Assortimentslider-bomen-rechts.png"
                        class="rounded" alt="Cinque Terre" height="450"
                        width="350">
            </div>
        </div>
    </div>
</div>
</body>
</html>