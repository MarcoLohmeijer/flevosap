<!DOCTYPE html>
<html lang="en">
<?php

use src\controllers\UserController;
use src\enums\Routes;

include_once "src/controllers/UserController.php";
include_once "src/components/head.php";

$userController = new UserController();
?>
<?php include_once "src/components/navbar.php"; ?>
<?php include_once "src/components/banner.php"; ?>
<?php include_once "src/components/CheckIfAdmin.php"; ?>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-2">
                    id
                </div>
                <div class="col-sm-3">
                    Gebruikers naam
                </div>
                <div class="col-sm-5">
                    E-mail
                </div>
                <div class="col-sm-2">
                    Delete
                </div>
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>

			<?php
			$users = $userController->showAll();

			foreach ($users as $user) {
				if ($user['id'] == $_SESSION['user_id']) {
					continue;
				}
				echo "
              <div class='row' >
                 <div class=\"col-sm-2\">
                    " . $user ['id'] . "
                </div>
                <div class=\"col-sm-3\">
                    " . $user ['username'] . "
                </div>
                <div class=\"col-sm-5\">
                    " . $user ['email'] . ">
                </div>
                <div class=\"col-sm-2\">
                     <form method='post' action =' " . Routes::ADMIN_DELETE_USER_ACTION . " '>
                       <input type='hidden' name='id' value='" . $user['id'] . "'>
                      <button type ='action' class=\"btn btn-primary\" name=\"delete\">Verwijderen</button>
                    </form>
                </div>
                <div class='col-sm-12'><hr></div>
             </div>";
			}
			?>

			<?php include_once "src/components/errorMessage.php"; ?>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
<style>
    input[type=text], select {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    input[type=submit] {
        width: 50%;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    input[type=submit]:hover {
        background-color: #45a049;
    }
</style>
</body>
</html>