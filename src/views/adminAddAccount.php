<?php

use src\enums\Routes;

?>
<!DOCTYPE html>
<html lang="en">
<?php include_once "src/components/head.php"; ?>
<body>
<?php include_once "src/components/navbar.php"; ?>
<div class="container-fluid">
    <?php include_once "src/components/banner.php"; ?>
    <?php include_once "src/components/CheckIfAdmin.php"; ?>
    <div class="row">

        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <h1>Voeg nieuwe gebruiker toe aan uw website</h1>
            <hr/>
            <h1>Register</h1>
            <form action="<?php echo Routes::ADMIN_ADD_ACCOUNT_ACTION ?>" method="post">
                <div class="form-group">
                    <label for="username">Username:</label>
                    <input name="username" type="text" class="form-control" id="username">
                </div>

                <div class="form-group">
                    <label for="email">E-mail:</label>
                    <input name="email" type="email" class="form-control" id="email">
                </div>

                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input name="password" type="password" class="form-control" id="pwd">
                </div>

                <div class="form-group">
                    <label for="pwd2">Repeat password:</label>
                    <input name="password2" type="password" class="form-control" id="pwd2">
                </div>

                <div class="form-group">
                    <label class="radio-inline"><input type="radio" value="user" name="role"
                                                       checked>User</label>
                    <label class="radio-inline"><input type="radio" value="admin" name="role">Admin</label>
                </div>

                <div class="float-right">
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>

                <?php include_once "src/components/errorMessage.php"; ?>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
</body>
</html>
