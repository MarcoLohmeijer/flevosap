<!DOCTYPE html>
<html lang="en">
<?php

use src\enums\Routes;

include_once "src/components/head.php"; ?>
<body>

<?php include_once "src/components/navbar.php"; ?>

<div class="container-fluid">
	<?php include_once "src/components/banner.php"; ?>
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<h1>Adress</h1>
			<form action="<?php echo Routes::CREATE_ADDRESS ?>" method="post">
				<div class="form-group">
					<label for="street">Straat:</label>
					<input name="street" type="text" class="form-control" id="street">
				</div>
				<div class="form-group">
					<label for="number">Huisnummer:</label>
					<input name="number" type="number" class="form-control" id="number">
				</div>
				<div class="form-group">
					<label for="addition">Toevoeging:</label>
					<input name="addition" type="text" maxlength="10" class="form-control" id="addition">
				</div>
				<div class="form-group">
					<label for="postcode">Postcode:</label>
					<input name="postcode" type="text" maxlength="6" class="form-control" id="postcode">
				</div>
				<div class="form-group">
					<label for="city">Stad:</label>
					<input name="city" type="text" class="form-control" id="city">
				</div>
				<div class="float-right">
					</h4>
					<button type="submit" class="btn btn-primary">Register</button>
					<?php include_once "src/components/errorMessage.php"; ?>
				</div>
			</form>

		</div>
		<div class="col-sm-4"></div>
	</div>
</div>
</body>
</html>