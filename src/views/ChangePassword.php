<!DOCTYPE html>
<?php use src\enums\Routes;

include_once 'src/components/head.php'; ?>

<body>
<?php include_once 'src/components/navbar.php'; ?>
<?php include_once "src/components/banner.php"; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <form action="<?php echo Routes::UpdatePassword ?>" method="POST">
                <h1 class="text-center">Change password</h1>

                <div class="form-group">
                    <label for="pwd">Current password:</label>
                    <input name="password" type="password" class="form-control" id="pwd">
                </div>

                <div class="form-group">
                    <label for="new-pwd">New Password:</label>
                    <input name="new-password" type="password" class="form-control" id="new-pwd"
                           pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                           title="Moet minimaal 1 kleine letter, 1 hoofdletter 1 nummer en minimaal 8 karakters lang zijn"">
                </div>

                <div class="form-group">
                    <label for="new-pwd2">Repeat new Password:</label>
                    <input name="new-password2" type="password" class="form-control" id="new-pwd2">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo Routes::USER_SETTINGS ?>" class="btn btn-primary float-right" role="button">Return
                    to previous page</a>
            </form>
			<?php include_once "src/components/errorMessage.php"; ?>
        </div>
        <div class="col-4"></div>
    </div>
</div>
</body>
</html>