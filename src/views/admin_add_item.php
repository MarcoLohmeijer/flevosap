<?php

use src\enums\Routes;

?>
<!DOCTYPE html>
<html lang="en">
<?php include_once "src/components/head.php"; ?>
<body>
<?php include_once "src/components/navbar.php"; ?>
<div class="container-fluid">
    <?php include_once "src/components/banner.php"; ?>
    <?php include_once "src/components/CheckIfAdmin.php"; ?>

    <div class="row">
        <div class="col-sm-12" style="background-color: white;">
            <div class="container">
                <h1>Voeg nieuw product toe aan uw assortiment</h1>

                <hr/>
                <form method='post' action='<?php echo Routes::ADMIN_ACTION_ADD_ITEM ?>' enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">Naam Product :</label>
                        <input type="text" class="form-control" name="name" id="name" required="required"
                               placeholder="Naam Product"/>
                    </div>

                    <div class="form-group">
                        <label for="description">Omschrijving Product :</label>
                        <input type="text" class="form-control" name="description" id="description" required="required"
                               placeholder="Omschrijving product" maxlength="150"/>
                    </div>

                    <div class="form-group">
                        <label for="price">Prijs Product :</label>
                        <input type="number" class="form-control" name="price" id="price" required="required" step="0.01"
                               placeholder="Prijs product"/>
                    </div>

                    <div class="form-group">
                        <label for="image">Image:</label>
                        <input type="file" class="form-control-file border" name="image" id="image">
                    </div>

                    <button type="submit" class="btn btn-primary">Add</button>
                </form>
                <?php include_once "src/components/errorMessage.php"; ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>

