<!DOCTYPE html>
<?php include_once 'src/components/head.php' ?>

<body>
<?php include_once 'src/components/navbar.php' ?>
<div class="container-fluid">

    <?php include_once "src/components/banner.php" ?>

    <div class="row">
        <div class="col" style="background-color: white;">
            <div class="container"><img
                        src="src/img/side-tree.png"
                        class="rounded" alt="Cinque Terre" height="450"
                        width="350"></div>
        </div>
        <div class="col" style="background-color: white;">
            <h1 style="color:darkred">Liefhebbers van Flevosap: welkom!</h1>
            <p style="color:darkgreen">De vele variaties zorgen voor de ene fruitige verrassing na de andere. De pure
                smaak van één soort,
                of een spannende combinatie van twee of meer. Altijd 100 procent natuurlijk, dus zonder smaak- en
                conserveringsmiddelen. Waar je ook bent, met Flevosap heb je altijd de smaak te pakken!</p>
            <h1 style="color:darkred">Altijd en overal genieten</h1>
            <p style="color:darkgreen"> De grote flessen kom je tegen in je supermarkt. Dat is makkelijk meenemen, en
                kinderen zijn er dol op.
                Volwassenen trouwens ook. In de winkel vind je ook een handig klein flesje dat graag mee onderweg
                gaat. Zoals naar (sport)school of werk, een dagje uit, of in je rugzak op vakantie.

                Gezellig samen wat eten en drinken in een café, lunchroom of restaurant? Ook daar kun je genieten
                van de smaak van vers fruit. Om zeker te weten dat je de echte krijgt, bestel je niet zo maar een
                sapje, maar vraag je om Flevosap!</p>
        </div>
        <div class="col" style="background-color: white;">
            <div class="container"><img
                        src="src/img/side-tree-right.png"
                        class="rounded" alt="Cinque Terre" height="450"
                        width="350">
            </div>
        </div>
    </div>
</div>
</body>