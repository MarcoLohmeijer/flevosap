<!DOCTYPE html>
<html lang="en">
<?php

use src\enums\Routes;
use src\controllers\ItemController;

include_once "src/controllers/ItemController.php";
include_once "src/components/head.php";

$itemController = new ItemController();
?>
<?php include_once "src/components/navbar.php";?>
<?php include_once "src/components/banner.php";?>
<?php include_once "src/components/CheckIfAdmin.php";?>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-1">
                        id
                    </div>
                    <div class="col-sm-2">
                        Product naam
                    </div>
                    <div class="col-sm-3">
                        Product omschijving
                    </div>
                    <div class="col-sm-2">
                        Product prijs
                    </div>
                    <div class="col-sm-2">
                        Edit
                    </div>
                    <div class="col-sm-2">
                        Delete
                    </div>
                    <div class="col-sm-12"><hr></div>
                </div>
            <?php
        $items = $itemController->showAll();

        foreach ($items as $item){
            echo"
              <div class='row' >
                 <div class=\"col-sm-1\">
                    ".$item ['id']."
                </div>
                <div class=\"col-sm-2\">
                    ".$item ['name']."
                </div>
                <div class=\"col-sm-3\">
                    ".$item ['description']."
                </div>
                <div class=\"col-sm-2\">
                    ".$item ['price']."
                </div>
                <div class=\"col-sm-2\">
                    <form method='post' action='/edit-item'>
                        <input type='hidden' name='item_id' value='".$item['id']."'>
                        <button class=\"btn btn-primary\" name=\"edit\">Edit</button>
                    </form>
                </div>
                <div class=\"col-sm-2\">
                    <form method='post' action ='/action/delete_item'>
                      <input type='hidden' name='id' value='".$item['id']."'>
                      <button type ='action' class=\"btn btn-primary\" name=\"delete\">Verwijderen</button>
                    </form>
                
                </div>
             </div>";
            }
    ?>
                <?php include_once "src/components/errorMessage.php"; ?>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
<style>
    input[type=text], select {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    input[type=submit] {
        width: 50%;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }

    input[type=submit]:hover {
        background-color: #45a049;
    }
</style>
</body>
</html>

