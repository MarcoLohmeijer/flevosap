<?php

use src\controllers\ItemController;
use src\enums\App;
use src\enums\Routes;

include_once "src/enums/App.php";
include_once "src/enums/Routes.php";
include_once "src/controllers/ItemController.php";

$i = new ItemController();

$sort = null;
if (isset($_GET['sort'])) {
    $sort = $_GET['sort'];
}

switch ($sort) {
    case 'desc':
        $items = $i->getByDesc();
        break;
    case 'asc':
        $items = $i->getByAsc();
        break;
    default:
        $items = $i->showAll();
}
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php' ?>
<body>
<?php include_once 'src/components/navbar.php' ?>
<?php include_once 'src/components/banner.php' ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="btn-group float-right pt-4 pb-4">
                <form action="" method="get">
                    <input name="uri" value="<?php echo Routes::ADMIN_ITEMS_GET_FORM?>" type="hidden">
                    <input name="sort" value="asc" type="hidden">
                    <button type="submit" class="btn btn-primary">Sort A-Z</button>
                </form>
                <form action="" method="get">
                    <input name="uri" value="<?php echo Routes::ADMIN_ITEMS_GET_FORM?>" type="hidden">
                    <input name="sort" value="desc" type="hidden">
                    <button type="submit" class="btn btn-primary">Sort Z-A</button>
                </form>
            </div>
            <hr>
        </div>
        <div class="col-sm-2"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <hr>
            <?php include_once "src/components/errorMessage.php"; ?>

            <div class='row'>

                <?php

                foreach ($items as $item) {
                    echo "
                    <div class=\"col-sm-4\">
                        <div class=\"card-deck\">
                            <div class=\"card\" >
                                <div class=\"card-body\">
                                    <h4 class=\"card-title\">" . $item['name'] . "</h4>
                                    <p class=\"card-text\">" . $item['description'] . "</p>
                                    <p class=\"card-text\">" . $item['price'] . "</p>
                                    <div class=\"btn-group\">
                                        <form action='' method='get'>
                                            <input type='hidden' name='uri' value='" . Routes::EDIT_ITEM_FORM . "'>
                                            <input type='hidden' name='item_id' value='" . $item['id'] . "'>
                                            <button type=\"submit\" class=\"btn btn-success\">Edit</button>
                                        </form>
                                        <form action='" . Routes::ADMIN_DELETE_ITEM . "' method='POST'>
                                            <input type='hidden' name='item_id' value='" . $item['id'] . "'>
                                            <button type=\"submit\" class=\"btn btn-danger\">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>";
                }
                ?>

            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
</body>
</html>

