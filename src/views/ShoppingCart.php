<?php

use src\controllers\ShoppingCartController;
use src\enums\App;
use src\enums\Routes;
use src\helpers\Redirect;

include_once "src/enums/App.php";
include_once "src/enums/Routes.php";

$w = new ShoppingCartController();

if (isset($_SESSION['user_id']) === false) {
    Redirect::to(Routes::LOGIN, "message=unauthorized");
}
?>

<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php' ?>
<body>
<?php include_once 'src/components/navbar.php' ?>
<div class="container-fluid">
    <?php include_once 'src/components/banner.php' ?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <hr>
            <?php
            $number = 0;
            foreach ($w->getItems($_SESSION['user_id']) as $item) {
                $w->addToTotalCosts($item['price'] * $item['amount']);
                $number += 1;
                echo
                    "<div class='row'>
                                    <div class='col-sm-2'>
                                        <img class=\"card-img-top\" src=\"src/item_images/" . $item['img_url'] . "\" alt=\"Card image\" style=\"max-width:100px; max-height:100px;\">
                                    </div>
                                    
                                    <div class='col-sm-4'>
                                        <b>" . $item['name'] . "</b>
                                        <p>" . substr($item['description'], 0, 40) . "</p>
                                    </div>
                                    
                                    <div class='col-sm-2'>
                                        <p>Prijs: &euro; " . number_format((float)$item['price'], 2, '.', '') . "</p>
                                    </div>
                                    
                                    <div class='col-sm-2'>
                                        <p>Aantal: " . $item['amount'] . "</p>
                                        <div class=\"btn-group\">
                                            <button type='submit' class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#edit-amount-modal" . $number . "\"><i class=\"fas fa-edit\"></i></button>
                                            <form method='post' action='" . Routes::DELETE_FROM_CART . "'>
                                                <input type='hidden' name='item_id' value='" . $item['item_id'] . "'>
                                                <input type='hidden' name='cart_id' value='" . $item['cart_id'] . "'>
                                                <button type=\"submit\" class=\"btn btn-danger\"><i class=\"fas fa-trash-alt\"></i></button>
                                            </form>
                                        </div>
                                    </div>

                                    <div class='col-sm-2'>
                                        <p>Totaal prijs: &euro; " . number_format((float)$item['price'] * $item['amount'], 2, '.', '') . "</p>
                                    </div>
                                   
                                   <!-- modal -->
                                   <div class=\"modal fade\" id=\"edit-amount-modal" . $number . "\">
                                    <div class=\"modal-dialog modal-dialog-centered\">
                                        <div class=\"modal-content\">
                                            <!-- Modal Header -->
                                            <div class=\"modal-header\">
                                              <h4 class=\"modal-title\">Edit item</h4>
                                              <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
                                            </div>
                                            <!-- Modal body -->
                                            <div class=\"modal-body\">
                                                <form method='post' action='" . Routes::UPDATE_ITEM_FROM_CART . "'>
                                                    <div class=\"form - group\">
                                                        <input type='number' name='amount' value='" . $item['amount'] . "'>
                                                        <input type='hidden' name='item_id' value='" . $item['item_id'] . "'>
                                                        <input type='hidden' name='cart_id' value='" . $item['cart_id'] . "'>
                                                    </div>

                                                    <button type='submit' class=\"btn btn-primary\"><i class=\"fas fa - edit\"></i> Edit</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                         <hr>";
            }
            ?>


            <div class="float-right">
                <p style="margin-right: 10px; display: inline-block;">
                    totaal: <?php echo number_format((float)$w->getTotalCosts(), 2, '.', ''); ?> </p><a
                        href="<?php echo Routes::COMPARE_USER_ADDRESSES ?>">
                    <button type="button" class="btn btn-primary">Checkout</button>
                </a>
            </div>
            <?php include_once "src/components/errorMessage.php"; ?>
        </div>
        <hr>

    </div>
    <div class="col-sm-2"></div>
</div>
</body>
</html>

