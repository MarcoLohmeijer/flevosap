<!DOCTYPE html>
<html lang="en">
<?php

use src\controllers\OrderController;

include_once "src/components/head.php";

$orderController = new orderController();
?>
<?php include_once "src/components/navbar.php"; ?>
<?php include_once "src/components/banner.php"; ?>


<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-1">
                    Order -ID
                </div>
                <div class="col-sm-2">
                    Klant
                </div>
                <div class="col-sm-2">
                    Productnaam
                </div>
                <div class="col-sm-1">
                    Aantal
                </div>
                <div class="col-sm-3">
                    Straat + huisnummer
                </div>
                <div class="col-sm-2">
                    Stad
                </div>
                <div class="col-sm-1">
                    Bezorgen
                </div>

                <div class="col-sm-12"></div>
            </div>
			<?php
			$orders = $orderController->showOrdersCustomers($_SESSION['user_id']);

			usort($orders, function ($elem1, $elem2) {
				return strcmp($elem1['order_id'], $elem2['order_id']);
			});

			$i = null;

			foreach ($orders as $order) {

				$deliver = $orderId = $username = $street = $city = '';

				if ($i != $order['order_id']) {
					echo "<hr>";
					$orderId = $order['order_id'];
					$username = $order['username'];
					$street = $order['street'] . " " . $order['number'] . " " . $order['addition'];
					$city = $order['city'];
					$deliver = $order['deliver'] ? "Wordt bezorgd" : "Afhalen";
				}
				echo "
				<div class='row' >
                    <div class=\"col-sm-1\">
                        " . $orderId . " 
                    </div>
                    <div class=\"col-sm-2\">
                        " . $username . " 
                    </div>
                    <div class=\"col-sm-2\">
                        " . $order['name'] . "
                    </div>
                    <div class=\"col-sm-1\">
                        " . $order['amount'] . "
                    </div>
                    <div class=\"col-sm-3\">
                        " . $street . "
                    </div>
                    <div class=\"col-sm-2\">
                        " . $city . "
                    </div>
                    <div class='col-sm-1'>
                        " . $deliver . "
                    </div>
                </div>";
				$i = $order['order_id'];
			}
			?>
			<?php include_once "src/components/errorMessage.php"; ?>
        </div>
    </div
    <div class="col-sm-2"></div>
</div>
</body>