<?php

use src\enums\Routes;
use src\controllers\ItemController;

$itemController = new ItemController();

$item = $itemController->showItem(intval($_GET['item_id']));

?>

<!DOCTYPE html>
<?php include_once 'src/components/head.php'; ?>

<body>
<?php include_once 'src/components/navbar.php'; ?>
<?php include_once "src/components/banner.php"; ?>
<?php include_once "src/components/CheckIfAdmin.php"; ?>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h1 class="text-center">Edit item</h1>

            <form action="<?php echo Routes::EDIT_ITEM_ACTION ?>" method="post">
                <div class="form-group">
                    <label for="usr">Product name:</label>
                    <input value="<?php echo $item['name'] ?>" name="name" type="text" class="form-control"
                           id="name">
                </div>

                <div class="form-group">
                    <label for="email">description:</label>
                    <input value="<?php echo $item['description'] ?>" name="description" type="text"
                           class="form-control"
                           id="description">
                </div>
                <div class="form-group">
                    <label for="email">price:</label>
                    <input value="<?php echo $item['price'] ?>" name="price" type="text" class="form-control"
                           id="price">
                </div>
                <div class="form-group">
                    <input type='hidden' name='item_id' value=<?php echo $_GET['item_id'] ?>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-4"></div>
    </div>
</div>
</body>
</html>
