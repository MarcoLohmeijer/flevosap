<?php

use src\enums\Routes;
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once "src/components/head.php"; ?>
<body>
<?php include_once "src/components/navbar.php"; ?>
<div class="container-fluid">
    <?php include_once "src/components/banner.php"; ?>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h1>Register</h1>
            <form action="<?php echo Routes::CREATE_USER ?>" method="post">
                <div class="form-group">
                    <label for="username">Username:</label>
                    <input name="username" type="text" class="form-control" id="username">
                </div>
                <div class="form-group">
                    <label for="email">E-mail:</label>
                    <input name="email" type="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input name="password" type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                           title="Moet minimaal 1 kleine letter, 1 hoofdletter 1 nummer en minimaal 8 karakters lang zijn"
                    class="form-control" id="pwd" required>
                </div>
                <div class="form-group">
                    <label for="pwd2">Repeat password:</label>
                    <input name="password2" type="password"  class="form-control" id="pwd2">
                </div>
                <div class="float-right">
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>
                <div class="float-right">
                    <input type="hidden" name="role" value="user">
                </div>
            </form>
            <i>Al een account? <a href="<?php echo Routes::LOGIN ?>">Login</a></i>
            <?php include_once "src/components/errorMessage.php";?>
        </div>
        <div class="col-sm-4"></div>
    </div>
</div>
</body>
</html>