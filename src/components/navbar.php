<?php

use src\enums\Routes;

include_once "src/enums/Routes.php";
?>
<nav class="navbar navbar-expand-sm bg-light navbar-light">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="<?php echo Routes::HOME ?>">
        <img src="src/img/logo.png" alt="logo" style="width:40px;">
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Routes::HOME ?>">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Routes::ITEMS ?>">Producten</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Routes::SHOPPING_CART ?>">Winkelmand</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Routes::CONTACT ?>">Contact</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Routes::PRIVACY ?>">Privacy</a>
            </li>
            <?php
            if (isset($_SESSION['user_id'])) {
                echo "<li class='nav-item'>
                          <a class='nav-link' href='" . Routes::USER_SETTINGS . "'>Profiel</a>
                      </li>
                      <li class='nav-item'>
                          <a class='nav-link' href='" . Routes::ORDER_OVERVIEW . "'>Orders</a>
                      </li>
                      <li class='nav-item'>
                          <a class='nav-link' href='" . Routes::LOGOUT_USER . "'>Logout</a>
                      </li>";
            } else {
                echo "<li class='nav-item'>
                          <a class='nav-link' href='" . Routes::LOGIN . "'>Login</a>
                      </li>
                      <li class='nav-item'>
                          <a class='nav-link' href='" . Routes::REGISTER . "'>Register</a>
                      </li>";
            }
            if (isset($_SESSION['role'])) {
                if ($_SESSION['role'] == 'admin') {
                    echo "<li class='nav-item'>
                          <a class='nav-link' href='" . Routes::ADMIN_OVERVIEW . "'>Overview</a>
                      </li>";
                }
            }
            ?>

        </ul>
    </div>
</nav>