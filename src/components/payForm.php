<?php

use src\controllers\ShoppingCartController;
use src\enums\App;
use src\enums\Routes;

$w = new ShoppingCartController();

if (isset($_SESSION['user_id']) === false) {
    header("Location: ".App::URL.Routes::LOGIN."?message=unauthorized");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php' ?>
<body>
    <?php include_once 'src/components/navbar.php' ?>
    <div class="container-fluid">
        <?php include_once 'src/components/banner.php' ?>
        <div class="row">
            <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <form action="" method="post">
                        <div class="form-group">
                            <h2>Totaal bedrag = &euro;<?php echo $w->getCosts($_SESSION['user_id']); ?> </h2>
                            <a href="<?php echo Routes::MOVE_CART_TO_ORDER_ITEMS?>" class="btn btn-primary float-left" role="button">Betaal</a
                        </div>
                    </form>
                </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
</body>
</html>

