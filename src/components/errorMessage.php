<?php
if (isset($_GET['error'])) {
    echo "<div class=\"alert alert-danger alert-dismissible fade show mt-5\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            <strong>Error!</strong> " . $_GET['error'] . "
          </div>";
}
?>