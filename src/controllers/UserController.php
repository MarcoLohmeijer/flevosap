<?php

namespace src\controllers;

use Exception;
use src\enums\Routes;
use src\helpers\Redirect;
use src\models\User;
use src\repositories\UserRepository;

class UserController
{
    /** @var UserRepository */
    public $userRepo;

    /**
     * connect to database
     * @throws Exception
     */
    public function __construct()
    {
        $this->userRepo = new UserRepository(Database::connect());
    }

    /**
     * @param User $user
     *
     * @return User|false
     * @throws Exception
     */
    public function createUser(User $user)
    {
        return $this->userRepo->save($user)? $user: false;
    }

    /**
     * @param User $user
     *
     * @return User|false
     * @throws Exception
     */
    public function doUpdate(User $user)
    {
        return $this->userRepo->UpdateUser($user)? $user: false;
    }

    /**
     * @param User $user
     *
     * @return User|false
     * @throws Exception
     */
    public function updatePassword(User $user)
    {
        return $this->userRepo->UpdateUserPassword($user)? $user: false;
    }

    /**
     * @param User $user
     * @return mixed
     * @throws Exception
     */
    public function getByUsername(User $user)
    {
        return $this->userRepo->getByUsername($user->username);
    }

    /**
     * @param User $user
     *
     * @return array | false
     * @throws Exception
     */
    public function doLogin(User $user)
    {
        return $this->userRepo->getByUsername($user->username);
    }

    /**
     * @param int $user_id
     *
     * @return mixed|string
     * @throws Exception
     */
    public function getRole(int $user_id)
	{
		return $this->userRepo->getRoleByUserId($user_id);
	}

    /**
     * @param int $id
     *
     * @return bool
     * @throws Exception
     */
    public function doDelete(int $id)
    {
        $t = $this->userRepo->delete($id);

        return $this->userRepo->delete($id);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function showAll()
    {
        return $this->userRepo->showAll();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function compareUserAdresses()
	{
        if ( $this->userRepo->CheckUserAdress($_SESSION['user_id'])) {
            return 'src/views/orderPayForm.php';
        }

        return 'src/views/registerAdressForm.php';
	}

    /**
     * @return string view
     */
    public function getLogin()
    {
        return 'src/views/Login.php';
    }

    /**
     * @return string view
     */
    public function getRegister()
    {
        return 'src/views/Register.php';
    }

    /**
     * @return string view
     */
    public function getUpdate()
    {
        return 'src/views/UserSettings.php';
    }

    /**
     * @return string view
     */
    public function getChangePassword()
    {
        return 'src/views/ChangePassword.php';
    }

    /**
     * @return string view
     */
    public function getDelete()
	{
		return 'src/views/DeleteUser.php';
	}

    /**
     * handle logout
     */
    public function logout()
    {
        session_unset();
        session_destroy();

        Redirect::to(Routes::HOME, 'message=loggedOut');
    }
}