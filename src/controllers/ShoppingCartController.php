<?php

namespace src\controllers;

use Exception;
use src\factories\ShoppingCartFactory;
use src\models\CartItem;
use src\models\ShoppingCart;
use src\repositories\ShoppingCartRepository;

class ShoppingCartController
{
    /** @var float */
    private $total_price;

    /** @var ShoppingCartRepository */
    private $shoppingCartRepo;

    /**
     * ShoppingCartController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->shoppingCartRepo = new ShoppingCartRepository(Database::connect());
    }

    /**
     * @param int $user_id
     *
     * @return array
     * @throws Exception
     */
    public function getItems(int $user_id)
    {
        return $this->shoppingCartRepo->showAll($user_id);
    }

    /**
     * @param int $user_id
     * @param int $item_id
     *
     * @return array
     * @throws Exception
     */
    public function getItem(int $user_id, int $item_id)
    {
        return $this->shoppingCartRepo->getById($user_id, $item_id);
    }

    /**
     * @param int $user_id
     *
     * @return mixed
     * @throws Exception
     */
    public function getShoppingCartFromDb(int $user_id)
    {
        return $this->shoppingCartRepo->getShoppingCart($user_id);
    }

    /**
     * @param int $user_id
     *
     * @return ShoppingCart
     * @throws Exception
     */
    public function createShoppingCart(int $user_id)
    {
        $shoppingCart = ShoppingCartFactory::create($user_id);
        $this->shoppingCartRepo->saveCart($shoppingCart);

        return $shoppingCart;
    }

    /**
     * @param CartItem $cartItem
     *
     * @return CartItem | false
     * @throws Exception
     */
    public function setItem(CartItem $cartItem)
    {

        /**  Looks if the item is already in cart if so update the item */
        if ($item = $this->getItem($_SESSION['user_id'], $cartItem->item_id)) {
            /** Add amount to cart amount */
            $a = $cartItem->amount + $item['amount'];
            $cartItem->setAmount($a);
            return $this->editItem($cartItem);
        }

        return $this->shoppingCartRepo->saveCartItem($cartItem)? $cartItem : false;
    }

    /**
     * @param CartItem $item
     *
     * @return bool
     * @throws Exception
     */
    public function editItem(CartItem $item)
    {
        /** Amount can not be 0 */
        if ($item->amount == 0) {
            return $this->deleteItem($item);
        }

        return $this->shoppingCartRepo->edit($item);
    }

    /**
     * @param CartItem $cartItem
     *
     * @return bool
     * @throws Exception
     */
    public function deleteItem(CartItem $cartItem)
    {
        return $this->shoppingCartRepo->deleteItem($cartItem);
    }

    /**
     * @param float $costs
     */
    public function addToTotalCosts(float $costs = 0)
    {
        $this->total_price += $costs;
    }

    /**
     * @return float
     */
    public function getTotalCosts(): float
    {
        return $this->total_price ? $this->total_price : 0;
    }

    /**
     * @param $user_id
     * @return float|int
     * @throws Exception
     */
    public function getCosts($user_id)
    {
        $items =  $this->shoppingCartRepo->showAll($user_id);
        $total_price = 0;
        foreach ($items as $item){
            $total = $item['price'] * $item['amount'];
            $total_price += $total;
        }
        return number_format($total_price, 2, ',', '');
    }

    /**
     * @param $user_id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteRow($user_id){
        return $this->shoppingCartRepo->deleteRow($user_id);
    }

    /**
     * @return string view
     */
    public function getShoppingCart()
    {
        return 'src/views/ShoppingCart.php';
    }
}
