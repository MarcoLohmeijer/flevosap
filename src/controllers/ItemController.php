<?php

namespace src\controllers;

use Exception;
use src\models\Item;
use src\repositories\ItemRepository;

class ItemController
{
    /** @var ItemRepository */
    private $itemRepo;

    /**
     * connect to database
     * @throws Exception
     */
    public function __construct()
    {
        $this->itemRepo = new ItemRepository(Database::connect());
    }

    /**
     * @return array
     * @throws Exception
     */
    public function showAll()
    {
        return $this->itemRepo->showAll();
    }

    /**
     * @param $id
     *
     * @return mixed|string
     * @throws Exception
     */
    public function showItem(int $id)
    {
        return $this->itemRepo->showItem($id);
    }

    /**
     * @param Item $item
     * @param int $item_id
     *
     * @return false|Item
     */
    public function doEdit(Item $item, int $item_id)
    {
      return $this->itemRepo->edit($item_id, $item)? $item: false;
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws Exception
     */
    public function doDelete(int $id)
    {
        return $this->itemRepo->delete($id)? true: false;
    }

    /**
     * @param Item $item
     *
     * @return Item|false
     * @throws Exception
     */
    public function doInsert(Item $item)
    {
        return $this->itemRepo->insert($item)? $item: false;
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function checkIfItemIsUsed($id)
    {
        if ($this->itemRepo->getAllShoppingCartItems($id)) {
            return false;
        }

        if ($this->itemRepo->getAllOrderItems($id)) {
            return false;
        }

        return true;
    }

    /**
     * @return array|string
     */
    public function getByDesc()
    {
        return $this->itemRepo->getAllByDesc();
    }

    /**
     * @return array|string
     */
    public function getByAsc()
    {
        return $this->itemRepo->getAllByAsc();
    }

    /**
     * @return string
     */
    public function getItemList()
    {
        return 'src/views/item-list.php';
    }
}