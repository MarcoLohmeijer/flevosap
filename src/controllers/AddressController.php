<?php

namespace src\controllers;

use Exception;
use src\models\Address;
use src\repositories\AddressRepository;

class AddressController
{
    /** @var AddressRepository */
    public $addressRepo;

    /**
     * connect to database
     * @throws Exception
     */
    public function __construct()
    {
        $this->addressRepo = new AddressRepository(Database::connect());
    }

    /**
     * @param Address $address
     *
     * @return Address|false
     * @throws Exception
     */
    public function create(Address $address)
    {
        return $this->addressRepo->save($address)? $address: false;
    }
}

