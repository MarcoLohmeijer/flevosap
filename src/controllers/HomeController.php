<?php

namespace src\controllers;

class HomeController
{
    /**
     * @return string
     */
    public function getHome()
    {
        return 'src/views/home.php';
    }

    /**
     * @return string
     */
    public function getPrivacy()
    {
        return 'src/views/privacybeleid.php';
    }

    /**
     * @return string
     */
    public function getContact()
    {
        return 'src/views/Contact.php';
    }
}