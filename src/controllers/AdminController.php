<?php

namespace src\controllers;

class AdminController
{
    /**
     * @return string
     */
    public function getDelete()
    {
        return 'src/views/admin_delete_user.php';
    }

    /**
     * @return string
     */
    public function getAddItem()
    {
        return 'src/views/admin_add_item.php';
    }

    /**
     * @return string
     */
    public function getAdminOverview()
    {
        return 'src/views/adminOverview.php';
    }

    /**
     * @return string
     */
    public function getAddAccount()
    {
        return 'src/views/adminAddAccount.php';
    }

    /**
     * @return string
     */
    public function getOrderOverview()
    {
        return 'src/views/admin_order_overview.php';
    }

    /**
     * @return string
     */
    public function getEditItem()
    {
        return 'src/views/edititem.php';
    }

    /**
     * @return string
     */
    public function getItemList()
    {
        return 'src/views/admin_item_list.php';
    }
}