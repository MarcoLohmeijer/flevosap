<?php

namespace src\controllers;

use Exception;
use src\repositories\OrderRepository;

class OrderController
{
    /**
     * @var OrderRepository
     */
    private $orderRepo;


    /**
     * ShoppingCartController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->orderRepo = new OrderRepository(Database::connect());
    }

    /**
     * @return array|string
     * @throws Exception
     */
    public function showAll()
	{
	    return $this->orderRepo->showAll();
    }

    /**
     * @param $user_id
     * @return array
     * @throws Exception
     */
    public function showOrdersCustomers($user_id)
    {
        return $this->orderRepo->showAllForCustomers($user_id);
    }

    /**
     * @param $user_id
     * @param $order_id
     *
     * @throws Exception
     */
    public function migrateToOrderItems($user_id, $order_id)
    {
        $cart_item = new ShoppingCartController();
        $items= $cart_item->getItems($user_id);
        foreach ($items as $item){
             $this->orderRepo->migrate($item['item_id'], $item['amount'], $order_id);
        }
    }

    /**
     * @param $user_id
     * @param $deliver
     * @throws Exception
     */
    public function setDeliver($user_id, $deliver){
        $date = date("Y-m-d");
        $this->orderRepo->setOrders($user_id, $date, $deliver);
    }

    /**
     * @param $user_id
     *
     * @return mixed|string
     * @throws Exception
     */
    public function getOrderId($user_id){
        return $this->orderRepo->getOrderId($user_id);
    }

    /**
     * @param $order_id
     *
     * @return bool
     * @throws Exception
     */
    public function getOrder($order_id)
    {
        return $this->orderRepo->getOrder($order_id);
    }

    /**
     * @return string
     */
    public function registerAddressForm()
	{
		return 'src/views/registerAdressForm.php';
	}

    /**
     * @return string
     */
	public function orderPayForm()
	{
		return 'src/views/orderPayForm.php';
	}

	public function getOrdersView()
    {
        return 'src/views/klant-orders.php';
    }
}

