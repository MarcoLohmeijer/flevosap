<?php


namespace src\enums;


class Routes
{
    const HOME = '?uri=home';
    const LOGIN = '?uri=login';
    const PRIVACY = '?uri=privacy';
    const ITEMS = '?uri=items';
    const REGISTER = '?uri=register';
    const CONTACT = '?uri=Contact';
    const SHOPPING_CART = '?uri=shopping-cart';
    const USER_SETTINGS = '?uri=Update-User';
    const CHANGE_PASSWORD = '?uri=change-password';
    const DELETE_USER_PAGE = '?uri=DeleteUser';
    const DELETE_USER = '?uri=action/deleteUser';
    const DELETE_FROM_CART = '?uri=action/delete-item-from-cart';
    const UPDATE_USER = '?uri=action/update-user';
    const UpdatePassword = '?uri=action/UpdatePassword';
    const UPDATE_ITEM_FROM_CART = '?uri=action/update-item-from-cart';
    const ADD_ITEM_TO_CART = '?uri=action/add-item-to-cart';
    const CREATE_USER = '?uri=action/create-user';
    const LOGIN_USER = '?uri=action/login-user';
    const LOGOUT_USER = '?uri=action/logout-user';
    const CREATE_ADDRESS = '?uri=action/create-address';
    const ORDER_OVERVIEW = '?uri=orders';
    const PAY = '?uri=orderPayForm';
    const EDIT_ITEM= '?uri=edit-item';
    const EDIT_ITEM_ACTION = '?uri=action/edit-item';
    const MOVE_CART_TO_ORDER_ITEMS = '?uri=action/migrateToOrderItems';
    const ADMIN_DELETE_USER = '?uri=admin-delete-user';
	const ADMIN_DELETE_USER_ACTION = '?uri=admin-delete-user-action';
    const ADMIN_ADD_ITEM = '?uri=admin-add-item';
    const ADMIN_ACTION_ADD_ITEM = '?uri=actions/add_item';
    const ADMIN_ADD_ACCOUNT = '?uri=admin-add-account';
    const ADMIN_ITEM_LIST = '?uri=admin-item-list';
    const ADMIN_DELETE_ITEM = '?uri=action/delete_item';
    const COMPARE_USER_ADDRESSES = '?uri=action/comparUserAdresses';
    const ADMIN_OVERVIEW = '?uri=admin-overview';
    const ADMIN_ORDERS = '?uri=admin-order-overview';
    const ADMIN_ADD_ACCOUNT_ACTION = '?uri=admin-add-account-action';

    /** geen uri waarde komt in form */
    const EDIT_ITEM_FORM= 'edit-item';
    const ITEMS_GET_FORM = 'items';
    const ADMIN_ITEMS_GET_FORM = 'admin-item-list';
}