<?php

namespace src\actions;

use Exception;
use src\controllers\ItemController;
use src\enums\Routes;
use src\factories\ItemFactory;
use src\helpers\Redirect;
use src\models\Item;

class AdminEditItemAction extends ItemController
{
    /** @var Item */
    private $item;

    /** @var mixed */
    private $item_id;

    /**
     * AdminEditItemAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->item_id = $_POST['item_id'];
        $this->checkEmptyFields($_POST['name'], $_POST['description'], $_POST['price'], $_SESSION['user_id']);
        $this->item = ItemFactory::update($_POST['name'], $_POST['description'], $_POST['price'], $_SESSION['user_id']);
    }

    /**
     * handle item edit
     */
    public function edit()
    {
        if ($this->doEdit($this->item, $this->item_id)){
            Redirect::to(Routes::EDIT_ITEM, "item_id=".$this->item_id."&message=successful");
        }

        Redirect::to(Routes::EDIT_ITEM, "item_id=".$this->item_id."&error=edit-item-failed");
    }

    /**
     * @param $name
     * @param $description
     * @param $price
     * @param $userId
     */
    public function checkEmptyFields($name, $description, $price, $userId)
    {
        if (empty($name)) {
            Redirect::to(Routes::EDIT_ITEM, "item_id=".$this->item_id."&error=name-is-empty");
        }

        if (empty($description)) {
            Redirect::to(Routes::EDIT_ITEM, "item_id=".$this->item_id."&error=description-is-empty");
        }

        if (empty($price)) {
            Redirect::to(Routes::EDIT_ITEM, "item_id=".$this->item_id."&error=price-is-empty");
        }

        if (empty($userId)) {
            Redirect::to(Routes::EDIT_ITEM, "item_id=".$this->item_id."&error=userId-is-empty");
        }
    }
}