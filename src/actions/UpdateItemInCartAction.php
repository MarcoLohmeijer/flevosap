<?php

namespace src\actions;

use Exception;
use src\controllers\ShoppingCartController;
use src\enums\Routes;
use src\factories\ShoppingCartFactory;
use src\helpers\Redirect;
use src\models\CartItem;

class UpdateItemInCartAction extends ShoppingCartController
{
    /** @var CartItem  */
    private $item;

    /**
     * UpdateItemInCartAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkIfNegative($_POST['amount']);
        $this->item = ShoppingCartFactory::createItem($_POST['amount'], $_POST['item_id'], $_POST['cart_id']);
    }

    /**
     * handle item update
     * @throws Exception
     */
    public function update()
    {
        if ($this->editItem($this->item)) {
            Redirect::to(Routes::SHOPPING_CART, 'message=successful');
        }

        Redirect::to(Routes::SHOPPING_CART, 'error=Edit-item-failed');
    }

    /**
     * @param $amount
     */
    public function checkIfNegative($amount)
	{
		if ($amount < 0) {
			Redirect::to(Routes::SHOPPING_CART, 'error=amount-cant-be-negative');
		}
	}
}