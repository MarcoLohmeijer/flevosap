<?php

namespace src\actions;

use src\controllers\UserController;
use src\enums\Routes;
use src\factories\UserFactory;
use src\helpers\Redirect;
use src\models\User;

class LoginAction extends UserController
{
    /** @var User */
    private $user;

    /**
     * LoginAction constructor.
     */
    public function __construct()
    {
        parent::__Construct();
        $this->user = UserFactory::createLogin($_POST['username'], $_POST['password']);
    }

    /**
     * handle login
     */
    public function login()
    {
        $this->checkEmptyFields($this->user);

        if (!$databaseUser = $this->doLogin($this->user)) {
            Redirect::to(Routes::LOGIN, "error=user-not-found");
        }

        /** Checks if password is equal to the hashed password in the db */
        if (password_verify($this->user->password, $databaseUser['password'])) {
            $_SESSION["user_id"] = $databaseUser['id'];
            $_SESSION["username"] = $databaseUser['username'];
            $_SESSION["email"] = $databaseUser['email'];
            $_SESSION['role'] = implode("",$this->getRole($databaseUser['id']));

            Redirect::to(Routes::HOME, "message=success");
        }

        Redirect::to(Routes::LOGIN, "error=login-went-wrong");
    }

    /**
     * @param User $user
     */
    private function checkEmptyFields(User $user)
    {
        if (empty($user->username)) {
            Redirect::to(Routes::LOGIN, "error=username-is-empty");
        }

        if (empty($user->password)) {
            Redirect::to(Routes::LOGIN, "error=password-is-empty");
        }
    }
}