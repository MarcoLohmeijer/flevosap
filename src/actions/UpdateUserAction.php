<?php

namespace src\actions;

use src\controllers\UserController;
use src\enums\Routes;
use src\factories\UserFactory;
use src\helpers\Redirect;
use src\models\User;

class UpdateUserAction extends UserController
{
    /** @var User */
    private $user;

    /**
     * UpdateUserAction constructor.
     */
    public function __construct()
    {
        parent::__Construct();
        $this->user = UserFactory::createUpdate($_POST['username'], $_POST['email'], $_POST['password'], $_SESSION['user_id']);
    }

    /**
     * handle update
     */
    public function update()
    {
        $this->checkEmptyFields();

        $databaseUser = $this->getByUsername($this->user);

        /** Checks if password is equal to the hashed password in the db */
        if (password_verify($this->user->password, $databaseUser['password'])) {

            /** Update the user */
            if ($updatedUser = $this->doUpdate($this->user)) {
                $_SESSION['username'] = $updatedUser->username;
                $_SESSION['email'] = $updatedUser->email;
                Redirect::to(Routes::HOME, 'message=successful');
            }
        }

        Redirect::to(Routes::USER_SETTINGS, 'error=password-incorrect');
    }

    /**
     * handle empty fields
     */
    private function checkEmptyFields()
    {
        if (empty($this->user->username)) {
            Redirect::to(Routes::USER_SETTINGS, 'error=username-is-empty');
        }

        if (empty($this->user->email)) {
            Redirect::to(Routes::USER_SETTINGS, 'error=email-is-empty');
        }

        if (empty($this->user->password)) {
            Redirect::to(Routes::USER_SETTINGS, 'error=password-is-empty');
        }
    }
}