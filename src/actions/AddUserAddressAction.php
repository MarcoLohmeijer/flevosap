<?php

namespace src\actions;

use src\controllers\AddressController;
use src\enums\Routes;
use src\factories\AddressFactory;
use src\helpers\Redirect;
use src\models\Address;

class AddUserAddressAction extends AddressController
{
    /** @var Address  */
    private $address;

    /**
     * AddUserAddressAction constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkIfEmpty($_POST['street'], $_POST['number'], $_POST['postcode'], $_POST['city']);
        $this->address = AddressFactory::create($_POST['street'], $_POST['number'], $_POST['addition'],  $_POST['postcode'], $_POST['city'], $_SESSION['user_id']);
    }

    /**
     * Handle saving address
     */
    public function add()
    {
        if ($this->create($this->address)) {
            Redirect::to(Routes::PAY, 'message=success');
        }

        Redirect::to(Routes::COMPARE_USER_ADDRESSES, 'error=saving-address-went-wrong');
    }

    /**
     * @param $street
     * @param $number
     * @param $postcode
     * @param $city
     */
    public function checkIfEmpty($street, $number, $postcode, $city)
    {
        if (empty($street)) {
            Redirect::to(Routes::COMPARE_USER_ADDRESSES, 'error=street-is-empty');
        }

        if (empty($number)) {
            Redirect::to(Routes::COMPARE_USER_ADDRESSES, 'error=number-is-empty');
        }

        if (empty($postcode)) {
            Redirect::to(Routes::COMPARE_USER_ADDRESSES, 'error=postcode-is-empty');
        }

        if (empty($city)) {
            Redirect::to(Routes::COMPARE_USER_ADDRESSES, 'error=city-is-empty');
        }
    }
}