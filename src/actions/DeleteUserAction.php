<?php

namespace src\actions;

use src\controllers\UserController;
use src\enums\Routes;
use src\factories\UserFactory;
use src\helpers\Redirect;
use src\models\User;

class DeleteUserAction extends UserController
{
    /** @var User */
    private $user;

    /**
     * DeleteUserAction constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->user = UserFactory::createDeleteUser($_SESSION['username'], $_POST['password'], $_SESSION['user_id']);
    }

    /**
     * handle delete user
     */
    public function delete()
    {
        $this->checkEmptyFields();

        $databaseUser = $this->getByUsername($this->user);

        /** Checks if password is equal to the hashed password in the db */
        if (password_verify($this->user->password, $databaseUser['password'])) {

            /** Update the user */
            if ($user = $this->doDelete($this->user)) {
                session_unset();
                session_destroy();

                Redirect::to(Routes::HOME, 'message=Goodbye');
            }
        }

        Redirect::to(Routes::DELETE_USER_PAGE, 'error=password-is-not-correct');
    }

    /**
     * handle empty fields
     */
    private function checkEmptyFields()
    {
        if (empty($_POST['password'])) {
            Redirect::to(Routes::DELETE_USER_PAGE, 'error=password-is-empty');
        }
    }
}