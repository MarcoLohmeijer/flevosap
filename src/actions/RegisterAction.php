<?php

namespace src\actions;

use src\controllers\UserController;
use src\enums\Routes;
use src\factories\UserFactory;
use src\helpers\Hasher;
use src\helpers\Redirect;
use src\models\User;

class RegisterAction extends UserController
{
    /** @var User  */
    private $user;

    /**
     * RegisterAction constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->user = UserFactory::create($_POST['username'], $_POST['password'], $_POST['email'], $_POST['role']);
    }

    /**
     * Handle register
     */
    public function register()
    {
        $this->checkEmptyFields($_POST['password2']);

        $this->user->setPassword(Hasher::hash($this->user->password));

        if ($user = $this->createUser($this->user)) {
            Redirect::to(Routes::LOGIN, "message=success");
        }

        Redirect::to(Routes::REGISTER, "error=register-went-wrong");
    }

    /**
     * @param string $password2
     */
    private function checkEmptyFields(string $password2)
    {
        if (empty($this->user->username)) {
            Redirect::to(Routes::REGISTER, "error=username-is-empty");
        }

        if (empty($this->user->email)) {
            Redirect::to(Routes::REGISTER, "error=email-is-empty");
        }

        if (empty($this->user->password)) {
            Redirect::to(Routes::REGISTER, "error=password-is-empty");
        }

        if (empty($password2)) {
            Redirect::to(Routes::REGISTER, "error=repeat-password-is-empty");
        }

        if ($this->user->password !== $password2) {
            Redirect::to(Routes::REGISTER, "error=passwords-not-the-same");
        }
    }
}