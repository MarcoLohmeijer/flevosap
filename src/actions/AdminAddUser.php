<?php

namespace src\actions;

use src\controllers\UserController;
use src\enums\Routes;
use src\factories\UserFactory;
use src\helpers\Hasher;
use src\helpers\Redirect;
use src\models\User;

class AdminAddUser extends UserController
{
    /** @var User  */
    private $user;

    /**
     * RegisterAction constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->user = UserFactory::create($_POST['username'], $_POST['password'], $_POST['email'], $_POST['role']);
    }

    /**
     * Handle register
     */
    public function add()
    {
        $this->checkEmptyFields($_POST['password2']);

        $this->user->setPassword(Hasher::hash($this->user->password));

        if ($user = $this->createUser($this->user)) {
            Redirect::to(Routes::ADMIN_ADD_ACCOUNT, "message=success");
        }

        Redirect::to(Routes::ADMIN_ADD_ACCOUNT, "error=register-went-wrong");
    }

    /**
     * @param string $password2
     */
    private function checkEmptyFields(string $password2)
    {
        if (empty($this->user->username)) {
            Redirect::to(Routes::ADMIN_ADD_ACCOUNT, "error=username-is-empty");
        }

        if (empty($this->user->email)) {
            Redirect::to(Routes::ADMIN_ADD_ACCOUNT, "error=email-is-empty");
        }

        if (empty($this->user->password)) {
            Redirect::to(Routes::ADMIN_ADD_ACCOUNT, "error=password-is-empty");
        }

        if (empty($password2)) {
            Redirect::to(Routes::ADMIN_ADD_ACCOUNT, "error=repeat-password-is-empty");
        }

        if ($this->user->password !== $password2) {
            Redirect::to(Routes::ADMIN_ADD_ACCOUNT, "error=passwords-not-the-same");
        }
    }
}