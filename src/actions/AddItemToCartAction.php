<?php

namespace src\actions;

use Exception;
use src\controllers\ShoppingCartController;
use src\enums\Routes;
use src\factories\ShoppingCartFactory;
use src\helpers\Redirect;
use src\models\CartItem;

class AddItemToCartAction extends ShoppingCartController
{
    /** @var mixed  */
    private $cart;

    /** @var CartItem */
    private $item;

    /**
     * AddItemToCartAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkIfLoggedIn();
        $this->cart = $this->checkCart($_SESSION['user_id']);
        $this->checkIfAmount($_POST['amount']);
        $this->item = ShoppingCartFactory::createItem($_POST['amount'], $_POST['item_id'], $this->cart['id']);
    }

    /**
     * Handles add item to cart
     * @throws Exception
     */
    public function add()
    {
        /** Create the item in the shopping cart */
        if ($this->setItem($this->item)) {
            Redirect::to(Routes::SHOPPING_CART, 'message=success');
        }

        Redirect::to(Routes::ITEMS, 'message=add-item-to-cart-failed');
    }

    /**
     * check if user is logged in
     */
    public function checkIfLoggedIn()
    {
        /** You must be logged in to perform this script */
        if (isset($_SESSION['user_id']) === false) {
            Redirect::to(Routes::LOGIN, 'message=unauthorized');
        }
    }

    /**
     * @param $user_id
     *
     * @return mixed
     * @throws Exception
     */
    public function checkCart($user_id)
    {
        /** If the shopping cart not exists make one and grab it*/
        if (!$cart = $this->getShoppingCartFromDb($user_id)) {
            $this->createShoppingCart($user_id);
            return $this->getShoppingCartFromDb($user_id);
        }

        return $cart;
    }

    /**
     * @param $amount
     */
    public function checkIfAmount($amount)
    {
        /** Amount can not be 0 or negative */
        if ($amount <= 0) {
            Redirect::to(Routes::ITEMS, 'message==amount-can-not-be-null');
        }
    }
}