<?php

namespace src\actions;

use src\controllers\UserController;
use src\enums\Routes;
use src\factories\UserFactory;
use src\helpers\Hasher;
use src\helpers\Redirect;
use src\models\User;

class UpdatePasswordAction extends UserController
{
    /** @var User */
    private $user;

    /**
     * UpdatePasswordAction constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->user = UserFactory::createUpdatePassword($_SESSION['username'], $_SESSION['user_id']);
    }

    /**
     * handle update password
     */
    public function update()
    {
        $this->checkEmptyFields();

        if (!$databaseUser = $this->getByUsername($this->user)) {
            Redirect::to(Routes::CHANGE_PASSWORD, 'error=user-not-found');
        }

        /** Checks if password is equal to the hashed password in the db */
        if (password_verify($_POST['password'], $databaseUser['password'])) {

            $password = Hasher::hash($_POST['new-password']);
            $this->user->setPassword($password);

            /** Update the user password */
            if ($updatedUser = $this->updatePassword($this->user)) {
                Redirect::to(Routes::HOME, 'message=successful');
            }
        }

        Redirect::to(Routes::CHANGE_PASSWORD, 'error=update-password-failed');
    }

    /**
     * Check empty post fields
     */
    private function checkEmptyFields()
    {
        if (empty($_POST['new-password'])) {
            Redirect::to(Routes::CHANGE_PASSWORD, 'error=new-password-is-empty');
        }

        if (empty($_POST['new-password2'])) {
            Redirect::to(Routes::CHANGE_PASSWORD, 'error=repeat-new-password-is-empty');
        }

        /** Checks if the new passwords are the same */
        if ($_POST['new-password'] !== $_POST['new-password2']) {
            Redirect::to(Routes::CHANGE_PASSWORD, 'error=passwords-not-the-same');
        }
    }
}
