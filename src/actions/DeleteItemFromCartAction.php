<?php

namespace src\actions;

use Exception;
use src\controllers\ShoppingCartController;
use src\enums\Routes;
use src\factories\ShoppingCartFactory;
use src\helpers\Redirect;
use src\models\CartItem;

class DeleteItemFromCartAction extends ShoppingCartController
{
    /** @var CartItem  */
    private $cartItem;

    /**
     * DeleteItemFromCartAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->cartItem = ShoppingCartFactory::createItemToDelete($_POST['item_id'], $_POST['cart_id']);
    }

    /**
     * handle delete item from cart
     * @throws Exception
     */
    public function delete()
    {
        if ($this->deleteItem($this->cartItem)) {
            Redirect::to(Routes::SHOPPING_CART, 'message=successful');
        }

        Redirect::to(Routes::SHOPPING_CART, 'error=delete-item-failed');
    }
}