<?php

namespace src\actions;

use Exception;
use src\controllers\OrderController;
use src\controllers\ShoppingCartController;
use src\enums\Routes;
use src\helpers\Redirect;

class MigrateToOrderItems extends OrderController
{
    /**
     * @throws Exception
     */
	public function migrate()
	{
	    $deliver = isset($_POST['deliver'])? 1: 0;

		$shoppingCart = new ShoppingCartController();
		$this->setDeliver($_POST['user_id'], $deliver);
		$order_id = $this->getOrderId($_POST['user_id']);
		$this->migrateToOrderItems($_POST['user_id'], $order_id['MAX(id)']);
		$shoppingCart->deleteRow($_POST['user_id']);

		Redirect::to(Routes::ORDER_OVERVIEW, 'message=successful');
	}

}