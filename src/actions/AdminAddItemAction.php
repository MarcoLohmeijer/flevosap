<?php

namespace src\actions;

use Exception;
use src\controllers\ItemController;
use src\enums\Routes;
use src\factories\ItemFactory;
use src\helpers\Redirect;
use src\models\Item;

class AdminAddItemAction extends ItemController
{
    /** @var Item */
    private $item;

    /** @var string */
    private $imageName;

    /**
     * AdminAddItemAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->setImage();
        $this->item = ItemFactory::create($_POST['name'],$_POST['description'], $_POST['price'], $_SESSION['user_id'], $this->imageName);
    }

    /**
     * Handle add new item
     * @throws Exception
     */
    public function insert()
    {
        if ($this->doInsert($this->item) === false) {
            Redirect::to(Routes::ADMIN_ADD_ITEM, 'message=Failed');
        }

        Redirect::to(Routes::ADMIN_ADD_ITEM, 'message=Success');
    }

    /**
     * Handle saving image
     */
    private function setImage()
    {
        if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {

            /** uploaded image data */
            $thumbnailTmpPath = $_FILES['image']['tmp_name'];
            $thumbnailName = $_FILES['image']['name'];
            $thumbnailNameCmps = explode(".", $thumbnailName);
            $thumbnailExtension = strtolower(end($thumbnailNameCmps));

            $this->imageName = md5(time() . $thumbnailName) . '.' . $thumbnailExtension;

            /** directory where img will be stored */
            $uploadThumbnailDir = './src/item_images/';
            $upload_thumbnail_path = $uploadThumbnailDir . $this->imageName;

            /** set thumbnail in correct directory */
            if (!move_uploaded_file($thumbnailTmpPath, $upload_thumbnail_path)) {
                Redirect::to(Routes::ADMIN_ADD_ITEM, 'error=image-uploading-failed');
            }

            return true;
        }

        Redirect::to(Routes::ADMIN_ADD_ITEM, 'error=image-uploading-not-found');
    }
}