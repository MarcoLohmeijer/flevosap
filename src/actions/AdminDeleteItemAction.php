<?php

namespace src\actions;

use Exception;
use src\controllers\ItemController;
use src\enums\Routes;
use src\helpers\Redirect;

class AdminDeleteItemAction extends ItemController
{
    /**
     * AdminDeleteItemAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkEmptyFields($_POST['item_id']);
        $this->checkIfRelation($_POST['item_id']);
    }

    /**
     * Handle item delete
     * @throws Exception
     */
    public function delete()
    {
        if ($this->doDelete($_POST['item_id'])) {
            Redirect::to(Routes::ADMIN_ITEM_LIST, "message=successful");
        }

        Redirect::to(Routes::ADMIN_ITEM_LIST, "error=item-delete-failed");
    }

    /**
     * @param $id
     */
    public function checkEmptyFields($id)
    {
        if (empty($id)) {
            Redirect::to(Routes::ADMIN_ITEM_LIST, "error=item-id-is-empty");
        }
    }

    /**
     * @param $id
     * @throws Exception
     */
    public function checkIfRelation($id)
    {
        if (!$this->checkIfItemIsUsed($id)) {
            Redirect::to(Routes::ADMIN_ITEM_LIST, "error=item-is-in-use");
        }
    }
}