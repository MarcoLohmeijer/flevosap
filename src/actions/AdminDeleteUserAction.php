<?php

namespace src\actions;

use\src\controllers\AdminController;
use src\controllers\UserController;
use src\enums\Routes;
use src\factories\UserFactory;
use src\helpers\Redirect;
use src\models\User;

class AdminDeleteUserAction extends UserController
{
    /**
     * AdminDeleteUserAction constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * handle delete user
     */
    public function delete()
    {
        if ($u = $this->doDelete($_POST['id'])) {
            Redirect::to(Routes::ADMIN_DELETE_USER, 'message=gebruiker_is_verwijderd');
        }

        Redirect::to(Routes::ADMIN_DELETE_USER, 'error=gebruiker_kan_niet_worden_verwijderd');
    }
}