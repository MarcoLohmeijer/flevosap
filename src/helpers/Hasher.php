<?php

namespace src\helpers;

class Hasher
{
    /**
     * @param $value
     *
     * @return false|string
     */
    public static function hash($value)
    {
        return password_hash($value, PASSWORD_DEFAULT);
    }
}