<?php

namespace src\repositories;

use Exception;
use PDO;
use PDOException;
use src\models\User;

class UserRepository
{
	/** @var PDO */
	private $conn;

	/**
	 * UserRepository constructor.
	 * @param PDO $conn
	 */
	public function __construct(PDO $conn)
	{
		$this->conn = $conn;
	}

	/**
	 * @param User $user
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function save(User $user)
	{
		try {
			$stmt = $this->conn->prepare("INSERT INTO users (username, password, email)
                                            		VALUES (:username, :password, :email);
                                            		SET @user_id = (SELECT id FROM users WHERE username=:username);
                                            		INSERT INTO roles (name, user_id) 
                                            		VALUES (:role, @user_id)");

			$stmt->bindParam(':username', $user->username);
			$stmt->bindParam(':password', $user->password);
			$stmt->bindParam(':email', $user->email);
			$stmt->bindParam(":role", $user->role);

			return $stmt->execute();
		} catch (PDOException $e) {
			throw new Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * @param $username
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function getByUsername($username)
	{
		try {
			$stmt = $this->conn->prepare("SELECT * FROM users WHERE username=:username");
			$stmt->bindParam(':username', $username);
			$stmt->execute();

			return $stmt->fetch(PDO::FETCH_ASSOC);

		} catch (PDOException $e) {
			throw new Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * @param $user_id
	 *
	 * @return mixed|string
	 * @throws Exception
	 */
	public function getRoleByUserId($user_id)
	{
		try {
			$stmt = $this->conn->prepare("SELECT name FROM roles WHERE user_id=:user_id");
			$stmt->bindParam(':user_id', $user_id);
			$stmt->execute();

			return $stmt->fetch(PDO::FETCH_ASSOC);

		} catch (PDOException $e) {
			throw new Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * @param $user
	 * @param $id
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function UpdateUser(User $user)
	{
		try {
			$stmt = $this->conn->prepare("UPDATE users SET username = :username
                                           , email = :email 
                                            WHERE id = :id");
			$stmt->bindParam(':id', $user->getId());
			$stmt->bindParam(':username', $user->getUsername());
			$stmt->bindParam(':email', $user->getEmail());

			return $stmt->execute();

		} catch (PDOException $e) {
			throw new Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function CheckUserAdress($id)
	{
		try {
			$stmt = $this->conn->prepare(" SELECT * FROM addresses WHERE user_id=:id");
			$stmt->bindParam(':id', $id);
			$stmt->execute();
			return $stmt->fetch(PDO::FETCH_ASSOC);

		} catch (PDOException $e) {
			throw new Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * @param $user
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function UpdateUserPassword($user)
	{
		try {
			$stmt = $this->conn->prepare("UPDATE users SET password = :password
                                                   WHERE id = :user_id");
			$stmt->bindParam(':user_id', $user->id);
			$stmt->bindParam(':password', $user->password);

			return $stmt->execute();

		} catch (PDOException $e) {
			throw new Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * @param $id
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function delete($id)
	{

		try {
			$stmt = $this->conn->prepare("DELETE FROM roles WHERE user_id= :id;
												DELETE FROM addresses WHERE user_id= :id;
												DELETE FROM carts WHERE user_id= :id;
												DELETE FROM orders WHERE user_id= :id;
                                                DELETE FROM users WHERE id = :id");
			$stmt->bindParam(':id', $id);

			return $stmt->execute();

		} catch (PDOException $e) {
			throw new Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * @return array
	 * @throws Exception
	 */
	public function showAll()
	{
		try {
			$stmt = $this->conn->prepare("SELECT * FROM users");
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);

			return $stmt->fetchAll();
		} catch (PDOException $e) {
			throw new Exception("Error: " . $e->getMessage());
		}
	}
}