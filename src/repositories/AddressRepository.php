<?php


namespace src\repositories;

use Exception;
use PDO;
use PDOException;
use src\models\Address;

class AddressRepository
{
    /** @var PDO */
    private $conn;

    /**
     * ItemRepository constructor.
     * @param PDO $conn
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param Address $address
     *
     * @return bool
     * @throws Exception
     */
    public function save(Address $address)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO addresses (street , number, addition, postcode, city, user_id)
                                            VALUES (:street, :number, :addition, :postcode, :city, :user_id)");

            $stmt->bindParam(':street', $address->street);
            $stmt->bindParam(':number', $address->number);
            $stmt->bindParam(':addition', $address->addition);
            $stmt->bindParam(':postcode', $address->postcode);
            $stmt->bindParam(':city', $address->city);
            $stmt->bindParam(':user_id', $address->user_id);

            return $stmt->execute();

        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }
}