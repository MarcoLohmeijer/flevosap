<?php

namespace src\repositories;

use Exception;
use PDO;
use PDOException;
use src\models\Item;

class ItemRepository
{
    /** @var PDO */
    private $conn;

    /**
     * ItemRepository constructor.
     * @param PDO $conn
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @return array|string
     * @throws Exception
     */
    public function showAll()
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM items");
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }

    }

    /**
     * @param $id
     *
     * @return mixed|string
     * @throws Exception
     */
    public function showItem($id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM items WHERE id=:id");
            $stmt->bindParam(':id', $id);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw new Exception( "Error: " . $e->getMessage());
        }

    }

    /**
     * @param $id
     *
     * @return bool|string
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $stmt = $this->conn->prepare("DELETE FROM items WHERE id = :id");
            $stmt->bindParam(':id', $id);

            return $stmt->execute();

        } catch (PDOException $e) {
            throw new Exception("Error: ". $e->getMessage());
        }
    }

    /**
     * @param $item_id
     * @param Item $item
     *
     * @return bool|string
     * @throws Exception
     */
    public function edit($item_id, $item)
    {
        try {
            $stmt = $this->conn->prepare("UPDATE items SET name = :item_name, 
                                                price = :price, description = :description, updated_at = :updated_at
                                                WHERE id=:id");

            $stmt->bindParam(':item_name', $item->name);
            $stmt->bindParam(':description', $item->description);
            $stmt->bindParam(':price', $item->price);
            $stmt->bindParam(':updated_at', $item->updatedAt);
            $stmt->bindParam(':id', $item_id);

            return $stmt->execute();

        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $data
     *
     * @return bool|string
     * @throws Exception
     */
    public function insert($data)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO items (name, description, price, user_id, img_url)
                VALUES (:name, :description, :price, :user_id, :image_url)");

            $stmt->bindParam(':name', $data->name);
            $stmt->bindParam(':description', $data->description);
            $stmt->bindParam(':price', $data->price);
            $stmt->bindParam(':user_id', $data->userId);
            $stmt->bindParam(':image_url', $data->imageName);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @return array|string
     * @throws Exception
     */
    public function getAllByDesc()
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM items ORDER BY name DESC");
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @return array|string
     * @throws Exception
     */
    public function getAllByAsc()
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM items ORDER BY name ASC");
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $id
     *
     * @return array|false
     * @throws Exception
     */
     public function getAllShoppingCartItems($id)
     {
         try {
             $stmt = $this->conn->prepare("SELECT * FROM cart_items WHERE item_id = :id");
             $stmt->bindParam(':id', $id);

             $stmt->execute();
             $stmt->setFetchMode(PDO::FETCH_ASSOC);
             $value = $stmt->fetchAll();

             return empty($value) ? false: $value;
         } catch (PDOException $e) {
             throw new Exception($e->getMessage());
         }
     }

    /**
     * @param $id
     *
     * @return array|false
     * @throws Exception
     */
    public function getAllOrderItems($id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM order_items WHERE item_id = :id");
            $stmt->bindParam(':id', $id);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $value = $stmt->fetchAll();

            return empty($value) ? false: $value;
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }
}