<?php

namespace src\repositories;

use Exception;
use PDO;
use PDOException;
use src\models\CartItem;
use src\models\ShoppingCart;

class ShoppingCartRepository
{
    /** @var PDO */
    private $conn;

    /**
     * ShoppingCartRepository constructor.
     * @param PDO $conn
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param ShoppingCart $shoppingCart
     *
     * @return bool
     * @throws Exception
     */
    public function saveCart(ShoppingCart $shoppingCart)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO carts (user_id, updated_at)
                                            VALUES (:user_id, :updated_at)");
            $stmt->bindParam(':user_id', $shoppingCart->user_id);
            $stmt->bindParam(':updated_at', $shoppingCart->updated_at);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param CartItem $cartItem
     *
     * @return bool
     * @throws Exception
     */
    public function saveCartItem(CartItem $cartItem)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO cart_items (item_id, cart_id, amount)
                                            VALUES (:item_id, :cart_id, :amount)");
            $stmt->bindParam(':item_id', $cartItem->item_id);
            $stmt->bindParam(':cart_id', $cartItem->cart_id);
            $stmt->bindParam(':amount', $cartItem->amount);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param CartItem $cartItem
     *
     * @return bool
     * @throws Exception
     */
    public function edit(CartItem $cartItem)
    {
        try {
            $stmt = $this->conn->prepare("UPDATE cart_items SET amount = :amount, item_id = :item_id, cart_id = :cart_id 
                                            WHERE item_id=:item_id AND cart_id = :cart_id");
            $stmt->bindParam(':amount', $cartItem->getAmount());
            $stmt->bindParam(':item_id', $cartItem->getItemId());
            $stmt->bindParam(':cart_id', $cartItem->getCartId());

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param int $user_id
     *
     * @return array|string
     * @throws Exception
     */
    public function showAll(int $user_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM carts 
                                    JOIN cart_items ci on carts.id = ci.cart_id
                                    JOIN  items i on ci.item_id = i.id WHERE carts.user_id = :user_id");
            $stmt->bindParam(':user_id', $user_id);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param int $user_id
     * @param int $item_id
     *
     * @return mixed
     * @throws Exception
     */
    public function getById(int $user_id, int $item_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM carts 
                                    JOIN cart_items ci on carts.id = ci.cart_id
                                    JOIN  items i on ci.item_id = i.id WHERE carts.user_id = '$user_id' AND i.id = :item_id");
            $stmt->bindParam(':item_id', $item_id);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param int $user_id
     *
     * @return mixed
     * @throws Exception
     */
    public function getShoppingCart(int $user_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM carts WHERE user_id = :user_id");
            $stmt->bindParam(':user_id', $user_id);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param CartItem $cartItem
     *
     * @return bool
     * @throws Exception
     */
    public function deleteItem(CartItem $cartItem)
    {
        try {
            $stmt = $this->conn->prepare("DELETE FROM cart_items WHERE item_id = :item_id 
                                            AND cart_id = :cart_id");
            $stmt->bindParam(':item_id', $cartItem->item_id);
            $stmt->bindParam(':cart_id', $cartItem->cart_id);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $user_id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteRow($user_id){

        try {
            $stmt = $this->conn->prepare("DELETE cart_items FROM cart_items INNER JOIN carts
                                                    ON cart_items.cart_id = carts.id AND carts.user_id = :user_id;
                                                    DELETE FROM flevosap.carts WHERE user_id=:user_id");
            $stmt->bindParam(':user_id',$user_id);

            return $stmt->execute();
        }
        catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }
}
