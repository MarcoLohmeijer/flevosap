<?php


namespace src\repositories;

use Exception;
use PDO;
use PDOException;

class OrderRepository
{
    /**
     * @var PDO
     */
    private $conn;

    /**
     * OrderRepository constructor.
     * @param PDO $conn
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function showAll()
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM orders
 													INNER JOIN order_items ON orders.id = order_items.order_id
													INNER JOIN items on order_items.item_id = items.id
													INNER JOIN addresses on orders.user_id = addresses.user_id
													INNER JOIN users on orders.user_id = users.id");

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $user_id
     * @return array
     * @throws Exception
     */
    public function showAllForCustomers($user_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM orders
 													INNER JOIN order_items ON orders.id = order_items.order_id
													INNER JOIN items on order_items.item_id = items.id
													INNER JOIN addresses on orders.user_id = addresses.user_id
													INNER JOIN users on orders.user_id = users.id
													WHERE orders.user_id = :user_id");
            $stmt->bindParam(':user_id', $user_id);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $item_id
     * @param $amount
     * @param $order_id
     *
     * @return bool
     * @throws Exception
     */
    public function migrate($item_id, $amount, $order_id)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO order_items(item_id, amount, order_id)
                                            VALUES (:item, :amount, :order_id)");
            $stmt->bindParam(':item', $item_id);
            $stmt->bindParam(':amount', $amount);
            $stmt->bindParam(':order_id', $order_id);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }

    }

    /**
     * @param $user_id
     * @param $date
     * @param $deliver
     *
     * @return bool
     * @throws Exception
     *
     */
    public function setOrders($user_id, $date, $deliver)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO orders(user_id, date, deliver)
                                            VALUES(:user_id, :today, :deliver)");
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':today', $date);
            $stmt->bindParam(':deliver', $deliver);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $user_id
     *
     * @return mixed
     * @throws Exception
     */
    public function getOrderId($user_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT MAX(id) FROM orders WHERE user_id=:user_id");
            $stmt->bindParam(':user_id', $user_id);

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $order_id
     *
     * @return bool
     * @throws Exception
     */
    public function getOrder($order_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM orders WHERE id = :id");
            $stmt->bindParam(':id', $order_id);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }
}
