<?php

namespace src\factories;

use src\models\User;

class UserFactory
{
    /**
     * @param string $username
     * @param string $password
     * @param string $email
	 * @param string @role
	 *
     * @return User
     */
    public static function create(string $username, string $password, string $email, string $role): User
    {
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($password);
        $user->setEmail($email);
        $user->setRole($role);

        return $user;
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return User
     */
    public static function createLogin(string $username, string $password): User
    {
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($password);

        return $user;
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     * @param int $user_id
     *
     * @return User
     */
    public static function createUpdate(string $username, string $email, string $password, int $user_id)
    {
        $user = new User();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setId($user_id);

        return $user;
    }

    /**
     * @param string $username
     * @param int $id
     *
     * @return User
     */
    public static function createUpdatePassword(string $username, int $id)
    {
        $user = new User();
        $user->setUsername($username);
        $user->setId($id);

        return $user;
    }

    /**
     * @param string $username
     * @param string $password
     * @param int $id
     *
     * @return User
     */
    public static function createDeleteUser(string $username, string $password, int $id)
    {
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($password);
        $user->setId($id);

        return $user;
    }

    /**
     * @param string $username
     * @param string $email
     * @param int $id
     *
     * @return User
     */
    public static function createAdminDeleteUser(string $username, string $email, int $id)
    {
        $user = new User();
        $user->setId($id);
        $user->setUsername($username);
        $user->setEmail($email);

        return $user;
    }

}