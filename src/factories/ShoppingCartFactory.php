<?php

namespace src\factories;

use DateTime;
use src\models\CartItem;
use src\models\ShoppingCart;

class ShoppingCartFactory
{
    /**
     * @param int $user_id
     *
     * @return ShoppingCart
     * @throws \Exception
     */
    public static function create(int $user_id): ShoppingCart
    {
        $date = new DateTime();
        $shoppingCart = new ShoppingCart();
        $shoppingCart->setUserId($user_id);
        $shoppingCart->setUpdatedAt($date->format('Y-m-d'));

        return $shoppingCart;
    }

    /**
     * @param int $amount
     * @param int $item_id
     * @param int $cart_id
     *
     * @return CartItem
     */
    public static function createItem(int $amount, int $item_id, int $cart_id): CartItem
    {
        $cartItem = new CartItem();
        $cartItem->setAmount($amount);
        $cartItem->setItemId($item_id);
        $cartItem->setCartId($cart_id);

        return $cartItem;
    }

    /**
     * @param int $item_id
     * @param int $cart_id
     *
     * @return CartItem
     */
    public static function createItemToDelete(int $item_id, int $cart_id)
    {
        $cartItem = new CartItem();
        $cartItem->setItemId($item_id);
        $cartItem->setCartId($cart_id);

        return $cartItem;
    }
}