<?php

namespace src\factories;

use DateTime;
use Exception;
use src\models\Item;

class ItemFactory
{
    /**
     * @param string $name
     * @param string $description
     * @param float $price
     * @param int $userId
     * @param string $imageName
     *
     * @return Item
     * @throws Exception
     */
    public static function create(string $name, string $description, float $price, int $userId, string $imageName): Item
    {
        $date = new DateTime();
        $item = new Item();
        $item->setName($name);
        $item->setDescription($description);
        $item->setPrice($price);
        $item->setUserId($userId);
        $item->setImageName($imageName);
        $item->setCreatedAt($date->format('Y-m-d'));
        $item->setUpdatedAt($date->format('Y-m-d'));

        return $item;
    }

    /**
     * @param string $name
     * @param string $description
     * @param float $price
     * @param int $userId
     *
     * @return Item
     * @throws Exception
     */
    public static function update(string $name, string $description, float $price, int $userId): Item
    {
        $date = new DateTime();
        $item = new Item();
        $item->setName($name);
        $item->setDescription($description);
        $item->setPrice($price);
        $item->setUserId($userId);
        $item->setUpdatedAt($date->format('Y-m-d'));

        return $item;
    }

    /**
     * @param string $name
     * @param string $description
     * @param float $price
     * @param int $userId
     *
     * @return Item
     */
    public static function insert(string $name, string $description, float $price, int $userId): Item
    {
        $item = new Item();
        $item->setName($name);
        $item->setDescription($description);
        $item->setPrice($price);
        $item->setUserId($userId);

        return $item;
    }
}