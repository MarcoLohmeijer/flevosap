<?php


namespace src\factories;

use src\models\Address;

include_once "src/models/Address.php";

class AddressFactory
{
    public static function create($street, $number, $addition, $postcode, $city, $user_id)
    {
        $address = new Address();
        $address->setStreet($street);
        $address->setNumber($number);
        $address->setAddition($addition);
        $address->setPostcode($postcode);
        $address->setCity($city);
        $address->setUser_id($user_id);

        return $address;
    }
}