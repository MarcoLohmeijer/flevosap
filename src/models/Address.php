<?php

namespace src\models;

class Address
{
    /** @var string */
    public $street;

    /** @var int */
    public $number;

    /** @var string */
    public $addition;

    /** @var string */
    public $postcode;

    /** @var string */
    public $city;

    /** @var int */
    public $user_id;

    /**
     * @param $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @param $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @param $addition
     */
    public function setAddition($addition)
    {
        $this->addition = $addition;
    }

    /**
     * @param $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @param $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @param $user_id
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
    }
}