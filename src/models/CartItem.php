<?php


namespace src\models;


class CartItem
{
    /** @var int */
    public $amount;

    /** @var int */
    public $cart_id;

    /** @var int */
    public $item_id;

    /**
     * @param int $amount
     */
    public function setAmount(int $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $cart_id
     */
    public function setCartId(int $cart_id)
    {
        $this->cart_id = $cart_id;
    }

    /**
     * @return int
     */
    public function getCartId(): int
    {
        return $this->cart_id;
    }

    /**
     * @param int $item_id
     */
    public function setItemId(int $item_id)
    {
        $this->item_id = $item_id;
    }

    /**
     * @return int
     */
    public function getItemId(): int
    {
        return $this->item_id;
    }
}